/**
 * \file osci_config.h
 */

#ifndef _OSCI_CONFIG_H
#define _OSCI_CONFIG_H

/**
 * OSCI_DEBUG_UART - enable for debug output on uart
 */
#define OSCI_DEBUG_UART

#define OSCI_DEBUG_UART_BAUD 57600

#define BUILD_OSCI_CONTROLS_FIRMWARE

/**
 * BUILD_OSCI_CONTROLS_FIRMWARE_WITH_USB - enable USB (HID) support
 */
#define BUILD_OSCI_CONTROLS_FIRMWARE_WITH_USB

/**
 * BUILD_OSCI_CONTROLS_FIRMWARE_WITH_TWI - enable TWI/I2C slave transmitter
 */
#define BUILD_OSCI_CONTROLS_FIRMWARE_WITH_TWI

/*! \brief number of channels */
#define OSCI_CHANNELS 2

#endif /* _OSCI_CONFIG_H */

/* vim: set expandtab: */
