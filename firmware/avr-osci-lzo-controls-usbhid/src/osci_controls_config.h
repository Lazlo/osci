/**
 * \file osci_controls_config.h
 * \todo use shift registers using SPI
 */

#ifndef _OSCI_CONTROLS_CONFIG_H
#define _OSCI_CONTROLS_CONFIG_H

/*! \brief by defining OSCI_CONTROLS_USE_TIMER the default polling will be replaced by interrupt based timer calls */
//#define OSCI_CONTROLS_USE_TIMER

#define OSCI_CONTROLS_TIMER             2
#define OSCI_CONTROLS_ISR_TIMER_OVERFLOW        ISR( TIMER2_OVF_vect )

/*! \brief number of bytes to read from the parallel in / serial out shift register */
#define PISOSHR_READ_BYTES              3

/*! \brief position of the byte that contains the bits related to the button matrix */
#define PISOSHR_BUTTON_MATRIX_BYTE      0
/*! \brief position of rotary encoder high bits/byte */
#define PISOSHR_ROTARY_BYTE_HIGH        1
/*! \brief position of rotary encoder low bits/byte */
#define PISOSHR_ROTARY_BYTE_LOW	        2

//#define DEBUG_OSCI_CONTROLS_TICKS
//#define DEBUG_OSCI_CONTROLS_SERIAL_DATA
#define DEBUG_OSCI_CONTROLS_BUTTONS
#define DEBUG_OSCI_CONTROLS_KNOBS

#endif /* _OSCI_CONTROLS_CONFIG_H */

/* vim: set expandtab: */
