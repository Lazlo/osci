/**
 \file pisoshr.c
 */

#include "pisoshr.h"

#define PISOSHR_CLK_DELAY_US    1
#define PISOSHR_LOAD_DELAY_US   1

#define PISOSHR_CLK_init        PISOSHR_CLK_DDR |= ( 1 << PISOSHR_CLK )
#define PISOSHR_SH_LD_init      PISOSHR_SH_LD_DDR |= ( 1 << PISOSHR_SH_LD )
#define PISOSHR_Q_init          PISOSHR_Q_DDR &= ~(1<<PISOSHR_Q )

#define PISOSHR_CLK_high        PISOSHR_CLK_PORT |= ( 1 << PISOSHR_CLK )
#define PISOSHR_CLK_low         PISOSHR_CLK_PORT &= ~( 1 << PISOSHR_CLK )
#define PISOSHR_SH_LD_high      PISOSHR_SH_LD_PORT |= ( 1 << PISOSHR_SH_LD )
#define PISOSHR_SH_LD_low       PISOSHR_SH_LD_PORT &= ~( 1 << PISOSHR_SH_LD )

/* change CLK from low to high lets the clock advance one cycle (restore high when finished) */
// clock by making a low-to-high change on the CLK line
/* pull SH/LD low to shift a bit (restore high when finished) */

void pisoshr_init( struct pisoshr_t *shr )
{
        // setup pin directions
        PISOSHR_CLK_init;	// output
        PISOSHR_SH_LD_init;	// output
        PISOSHR_Q_init;		// input
        // set default levels
        PISOSHR_CLK_high;	// CLK output to high
        PISOSHR_SH_LD_high;
}

void pisoshr_load( struct pisoshr_t *shr )
{
//      PISOSHR_LOAD();
        PISOSHR_SH_LD_low;
        _delay_us( PISOSHR_LOAD_DELAY_US );
        PISOSHR_SH_LD_high;
}

uint8_t pisoshr_read_bit( struct pisoshr_t *shr )
{
        uint8_t bit_is_set;
        bit_is_set = ( PISOSHR_Q_PIN & ( 1 << PISOSHR_Q ) );
        // Clocking is accomplished by a low-to-high transition of the clock (CLK) input while SH/LD is held high and CLK INH is held low
        PISOSHR_CLK_low;
        _delay_us( PISOSHR_CLK_DELAY_US );
        PISOSHR_CLK_high;
        return bit_is_set;
}

uint8_t pisoshr_read_byte( struct pisoshr_t *shr )
{
        uint8_t b, i;
        b = 0;
        // While SH/LD is low, the parallel inputs to the register are enabled
        // independently of the levels of the CLK, CLK INH, or serial (SER) inputs.
        for( i = 0; i < 8; i++ ) {
                if( pisoshr_read_bit( shr ) )
                        b |= ( 1 << i );
        }
        return b;
}

/* vim: set expandtab: */
