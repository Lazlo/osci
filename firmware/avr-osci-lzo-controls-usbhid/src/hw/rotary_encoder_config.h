/**
 * \file rotary_encoder_config.h
 */

#ifndef _ROTARY_ENCODER_CONFIG_H
#define _ROTARY_ENCODER_CONFIG_H

#include <avr/io.h>

/*! \brief number of encoders connected */
#define RE_NUM			8

/*! \brief bit-width of int type used to store data and last data of rotary encoders */
#define RE_DATA_INTTYPE_WIDTH	16

/* IO
 * The output pins of encoders can either be connected directly to the pins of the mcu (parallel) or
 * to the pins of a "parallel in / serial out shift register" IC (e.g. 74xx165) that is connected to
 * the mcu (serial).
 * Either use:
 *      #define RE_IO_TYPE_PARALLEL
 * or
 *      #define RE_IO_TYPE_SERIAL
 */

#define RE_IO_TYPE_SERIAL

/* Tell the code if the rotary encoder is setup in active-low or active-high configuration.
 * Use (replace "n" with identifier):
 *      #define REn_ACTIVE_LOW	// active when level is GNC, default level VCC
 * or
 *      #define REn_ACTIVE_HIGH	// active when level is VCC, default level GND
 */

#define RE1_ACTIVE_LOW
#define RE2_ACTIVE_LOW
#define RE3_ACTIVE_LOW
#define RE4_ACTIVE_LOW
#define RE5_ACTIVE_LOW
#define RE6_ACTIVE_LOW
#define RE7_ACTIVE_LOW
#define RE8_ACTIVE_LOW

#if defined( RE_IO_TYPE_PARALLEL )

/* Use internal pull-up resistors or are
 * external ones in place? Use:
 *      #define USE_INTERNAL_PULLUPS
 */

//#define USE_INTERNAL_PULLUPS

/* Pin Mapping
 * Configure pins connected to the A and B
 * lines of the rotary encoder. Use:
 *      #define REn_
 *      #define REn_A_DDR
 *      #define REn_A_PORT
 *      #define REn_A_PIN
 */

#define RE1_A           PD2
#define RE1_A_DDR       DDRD
#define RE1_A_PORT      PORTD
#define RE1_A_PIN       PIND

#define RE1_B           PD3
#define RE1_B_DDR       DDRD
#define RE1_B_PORT      PORTD
#define RE1_B_PIN       PIND

#define RE2_A           PD4
#define RE2_A_DDR       DDRD
#define RE2_A_PORT      PORTD
#define RE2_A_PIN       PIND

#define RE2_B           PD5
#define RE2_B_DDR       DDRD
#define RE2_B_PORT      PORTD
#define RE2_B_PIN       PIND

/* Macros */
#define RE1_A_READ      ( RE1_A_PIN & ( 1 << RE1_A ) )
#define RE1_B_READ      ( RE1_B_PIN & ( 1 << RE1_B ) )

#define RE2_A_READ      ( RE2_A_PIN & ( 1 << RE2_A ) )
#define RE2_B_READ      ( RE2_B_PIN & ( 1 << RE2_B ) )

#elif defined( RE_IO_TYPE_SERIAL )

/* Rotary Encoders */

#define RE1     0
#define RE2     1
#define RE3     2
#define RE4     3
#define RE5     4
#define RE6     5
#define RE7     6
#define RE8     7

/* Define bit positions of A and B lines for every Rotary Encoders */

// Ch1 V/Div
#define RE1_A   0
#define RE1_B   1
// Ch2 V/Div
#define RE2_A   2
#define RE2_B   3
// Horizontal S/Div
#define RE3_A   4
#define RE3_B   5
// Trigger Level
#define RE4_A   6
#define RE4_B   7

// Ch1 Position
#define RE5_A   8
#define RE5_B   9
// Ch2 Position
#define RE6_A   10
#define RE6_B   11
// Horizontal Position
#define RE7_A   12
#define RE7_B   13
// General Purpose Knob
#define RE8_A   14
#define RE8_B   15

#define RE1_NAME        "Ch1 V/Div"
#define RE2_NAME        "Ch2 V/Div"
#define RE3_NAME        "Horizontal S/Div"
#define RE4_NAME        "Trigger Level"
#define RE5_NAME        "Ch1 Position"
#define RE6_NAME        "Ch2 Position"
#define RE7_NAME        "Horizontal Position"
#define RE8_NAME        "General Purpose Knob"

#else
#error "either RE_IO_TYPE_SERIAL or RE_IO_TYPE_PARALLEL must be defined."
#endif /* defined( RE_IO_TYPE_PARALLEL ) */

//ticks?

/*
 * Debugging
 * to enable a specific debugging feature use:
 *      #define RE_DEBUG_USING_LEDS
 *      #define RE_DEBUG_USING_UART
 *      #define RE_DEBUG_USING_UART_VERBOSE
 * NOTE: only one of the RE_DEBUG_USING_UART constants should be defined
 */

//#define RE_DEBUG_USING_LEDS
#define RE_DEBUG_USING_UART
//#define RE_DEBUG_USING_UART_VERBOSE

#ifdef RE_DEBUG_USING_LEDS

#define LED2            PD6
#define LED2_DDR        DDRD
#define LED2_PORT       PORTD

#define LED3            PD7
#define LED3_DDR        DDRD
#define LED3_PORT       PORTD

#define LED4            PB0
#define LED4_DDR        DDRB
#define LED4_PORT       PORTB

#define LED5            PB3
#define LED5_DDR        DDRB
#define LED5_PORT       PORTB

#endif /* RE_DEBUG_USING_LEDS */

#endif /* _ROTARY_ENCODER_CONFIG_H */

/* vim: set expandtab: */
