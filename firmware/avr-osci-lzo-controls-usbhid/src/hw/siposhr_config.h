/**
 * \file siposhr_config.h
 */

#ifndef _SIPOSHR_CONFIG_H
#define _SIPOSHR_CONFIG_H

#if defined(__AVR_ATmega8__)
// clock output
#define SIPOSHR_CP              PB5
#define SIPOSHR_CP_DDR          DDRB
#define SIPOSHR_CP_PORT         PORTB
#define SIPOSHR_CP_PIN          PINB

// serial output
#define SIPOSHR_DS1             PB4
#define SIPOSHR_DS1_DDR         DDRB
#define SIPOSHR_DS1_PORT        PORTB
#define SIPOSHR_DS1_PIN         PINB

// master reset output
#define SIPOSHR_MR              PB3
#define SIPOSHR_MR_DDR          DDRB
#define SIPOSHR_MR_PORT         PORTB
#define SIPOSHR_MR_PIN          PINB
#endif /* __AVR_ATmega8__ */

#if defined(__AVR_ATmega32__)
// clock output
#define SIPOSHR_CP              PD2
#define SIPOSHR_CP_DDR          DDRD
#define SIPOSHR_CP_PORT         PORTD
#define SIPOSHR_CP_PIN          PIND

// serial output
#define SIPOSHR_DS1             PD3
#define SIPOSHR_DS1_DDR         DDRD
#define SIPOSHR_DS1_PORT        PORTD
#define SIPOSHR_DS1_PIN         PIND

// master reset output
#define SIPOSHR_MR              PD4
#define SIPOSHR_MR_DDR          DDRD
#define SIPOSHR_MR_PORT         PORTD
#define SIPOSHR_MR_PIN          PIND
#endif /* __AVR_ATmega32__ */

#if defined(__AVR_ATmega644__)
// clock output
#define SIPOSHR_CP              PA2
#define SIPOSHR_CP_DDR          DDRA
#define SIPOSHR_CP_PORT         PORTA
#define SIPOSHR_CP_PIN          PINA

// serial output
#define SIPOSHR_DS1             PA3
#define SIPOSHR_DS1_DDR         DDRA
#define SIPOSHR_DS1_PORT        PORTA
#define SIPOSHR_DS1_PIN         PINA

// master reset output
#define SIPOSHR_MR              PA4
#define SIPOSHR_MR_DDR          DDRA
#define SIPOSHR_MR_PORT         PORTA
#define SIPOSHR_MR_PIN          PINA
#endif /* __AVR_ATmega644__ */

#endif /* _SIPOSHR_CONFIG_H */

/* vim: set expandtab: */
