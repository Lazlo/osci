/**
 \file siposhr.c
 */

#include "siposhr.h"

#define SIPOSHR_RESET_DELAY_US          1
#define SIPOSHR_SHIFT_DELAY_US          1
#define SIPOSHR_DATA_HOLD_DELAY_US      1

#define SIPOSHR_USE_GPIO        0

#if SIPOSHR_USE_GPIO == 0

#define SIPOSHR_CP_init         SIPOSHR_CP_DDR |= ( 1 << SIPOSHR_CP )
#define SIPOSHR_MR_init         SIPOSHR_MR_DDR |= ( 1 << SIPOSHR_MR )
#define SIPOSHR_DS1_init        SIPOSHR_DS1_DDR |= ( 1 << SIPOSHR_DS1 )

#define SIPOSHR_CP_high         SIPOSHR_CP_PORT |= ( 1 << SIPOSHR_CP )
#define SIPOSHR_CP_low          SIPOSHR_CP_PORT &= ~( 1 << SIPOSHR_CP )
#define SIPOSHR_MR_high         SIPOSHR_MR_PORT |= ( 1 << SIPOSHR_MR )
#define SIPOSHR_MR_low          SIPOSHR_MR_PORT &= ~( 1 << SIPOSHR_MR )
#define SIPOSHR_DS1_high        SIPOSHR_DS1_PORT |= ( 1 << SIPOSHR_DS1 )
#define SIPOSHR_DS1_low         SIPOSHR_DS1_PORT &= ~( 1 << SIPOSHR_DS1 )

#else
/*
#define SIPOSHR_CP_init         GpioPin_asOutput( shr->cp )
#define SIPOSHR_MR_init         GpioPin_asOutput( shr->mr )
#define SIPOSHR_DS1_init        GpioPin_asOutput( shr->ds1 )

#define SIPOSHR_CP_high         GpioPin_writeBit( shr->cp, 1 )
#define SIPOSHR_CP_low          GpioPin_writeBit( shr->cp, 0 )
#define SIPOSHR_MR_high         GpioPin_writeBit( shr->mr, 1 )
#define SIPOSHR_MR_low          GpioPin_writeBit( shr->mr, 0 )
#define SIPOSHR_DS1_high        GpioPin_writeBit( shr->ds1, 1 )
#define SIPOSHR_DS1_low         GpioPin_writeBit( shr->ds1, 0 )
*/
#endif

void siposhr_init( struct siposhr_t *shr )
{
        SIPOSHR_CP_init;
        SIPOSHR_MR_init;
        SIPOSHR_DS1_init;

        SIPOSHR_MR_high;
}

void siposhr_reset( struct siposhr_t *shr )
{
        SIPOSHR_MR_low;
        _delay_us( SIPOSHR_RESET_DELAY_US );
        SIPOSHR_MR_high;
}

void siposhr_shift( struct siposhr_t *shr )
{
        SIPOSHR_CP_high;
        _delay_us( SIPOSHR_SHIFT_DELAY_US );
        SIPOSHR_CP_low;
}

void siposhr_write( struct siposhr_t *shr, uint8_t b )
{
        if( b > 0 )
        {
                SIPOSHR_DS1_high;
                _delay_us( SIPOSHR_DATA_HOLD_DELAY_US );
                siposhr_shift( shr );
                SIPOSHR_DS1_low;
        }
/*
        uint8_t i;
        for( i = 0; i < 8; i++ ) {
                if( b & ( 1 << i ) )
                        SIPOSHR_DS1_PORT |= ( 1 << SIPOSHR_DS1 );
                siposhr_shift( );
                SIPOSHR_DS1_PORT &= ~( 1 << SIPOSHR_CP );
        }
*/
}

/* vim: set expandtab: */
