/**
 \file siposhr.h
 \brief Serial in / Parallel out Shift Register
 */

#ifndef _SIPOSHR_H
#define _SIPOSHR_H

#include <avr/io.h>
#include <util/delay.h>
#include <stdint.h> // for uint8_t

#include "siposhr_config.h"
#include "GpioPin.h" // for GpioPin_t

#ifndef SIPOSHR_CP
#error "SIPOSHR_CP is not defined!"
#endif

#ifndef SIPOSHR_CP_DDR
#error "SIPOSHR_CP_DDR is not defined!"
#endif

#ifndef SIPOSHR_CP_PORT
#error "SIPOSHR_CP_PORT is not defined!"
#endif

#ifndef SIPOSHR_CP_PIN
#error "SIPOSHR_CP_PIN is not defined!"
#endif

#ifndef SIPOSHR_DS1
#error "SIPOSHR_DS1 is not defined!"
#endif

#ifndef SIPOSHR_DS1_DDR
#error "SIPOSHR_DS1_DDR is not defined!"
#endif

#ifndef SIPOSHR_DS1_PORT
#error "SIPOSHR_DS1_PORT is not defined!"
#endif

#ifndef SIPOSHR_DS1_PIN
#error "SIPOSHR_DS1_PIN is not defined!"
#endif

#ifndef SIPOSHR_MR
#error "SIPOSHR_MR is not defined!"
#endif

#ifndef SIPOSHR_MR_DDR
#error "SIPOSHR_MR_DDR is not defined!"
#endif

#ifndef SIPOSHR_MR_PORT
#error "SIPOSHR_MR_PORT is not defined!"
#endif

#ifndef SIPOSHR_MR_PIN
#error "SIPOSHR_MR_PIN is not defined!"
#endif

struct siposhr_t {
        GpioPin_t       *cp,    // SIPOSHR_CP
                        *mr,    // SIPOSHR_MR
                        *ds1;   // SIPOSHR_DS1
};

/**
 \brief initialize IO
 */
void siposhr_init( struct siposhr_t *shr );

/**
 \brief reset
 */
void siposhr_reset( struct siposhr_t *shr );

/**
 \brief shift
 */
void siposhr_shift( struct siposhr_t *shr );

/**
 \brief write
 */
void siposhr_write( struct siposhr_t *shr, uint8_t b );

#endif /* _SIPOSHR_H */

/* vim: set expandtab: */
