/**
 \file pisoshr.h
 \brief Parallel in / Serial out Shift Register
 */

#ifndef _PISOSHR_H
#define _PISOSHR_H

#include <avr/io.h>
#include <util/delay.h>

#include "pisoshr_config.h"
#include "GpioPin.h" // for GpioPin_t

/* 74HC165 - (P)arallel-(I)n / (S)erial-(O)ut (Sh)ift (R)egister */

#ifndef PISOSHR_CLK
#error "PISOSHR_CLK needs to be defined!"
#endif

#ifndef PISOSHR_CLK_DDR
#error "PISOSHR_CLK_DDR needs to be defined!"
#endif

#ifndef PISOSHR_CLK_PORT
#error "PISOSHR_CLK_PORT needs to be defined!"
#endif

#ifndef PISOSHR_Q
#error "PISOSHR_Q needs to be defined!"
#endif

#ifndef PISOSHR_Q_DDR
#error "PISOSHR_Q_DDR needs to be defined!"
#endif

#ifndef PISOSHR_Q_PIN
#error "PISOSHR_Q_PIN needs to be defined!"
#endif

#ifndef PISOSHR_SH_LD
#error "PISOSHR_SH_LD needs to be defined!"
#endif

#ifndef PISOSHR_SH_LD_DDR
#error "PISOSHR_SH_LD_DDR needs to be defined!"
#endif

#ifndef PISOSHR_SH_LD_PORT
#error "PISOSHR_SH_LD_PORT needs to be defined!"
#endif

struct pisoshr_t {
        GpioPin_t       *clk,   // PISOSHR_CLK
                        *data,  // PISOSHR_Q
                        *load;  // PISOSHR_SH_LD
};

/**
 \brief initialize IO
 */
void pisoshr_init( struct pisoshr_t *shr );

/**
 \brief parallel load
 */
void pisoshr_load( struct pisoshr_t *shr );

/**
 \brief read serial bit
 */
uint8_t pisoshr_read_bit( struct pisoshr_t *shr );

/**
 \brief read serial byte
 */
uint8_t pisoshr_read_byte( struct pisoshr_t *shr );

#endif /* _PISOSHR_H */

/* vim: set expandtab: */
