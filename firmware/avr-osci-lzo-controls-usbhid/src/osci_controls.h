/**
 * \file osci_controls.h
 */

#ifndef _OSCI_CONTROLS_H
#define _OSCI_CONTROLS_H

#include "hw/button_matrix_config.h"	// for BUTTON_MATRIX_COLS for osci_controls_t struct
#include "hw/button_matrix.h"		// for bm_data_t t in osci_controls_t
#include "hw/rotary_encoder_config.h"	// ...
#include "hw/rotary_encoder.h"		// for re_t in osci_controls_t member re

/**
 * \brief data structure for controls state information
 */
typedef struct {
        /*! \brief Button Matrix */
        bm_t            bm;
        /*! \brief Rotary Encoders */
        re_t            re;
} osci_controls_t;

/**
 * \brief get key id of last pressed key
 */
unsigned char osci_controls_get_key( osci_controls_t *osci_controls );

/**
 * \brief Initialize controls - setup hardware and data structures
 */
void osci_controls_init( osci_controls_t *osci_controls );

/**
 * \brief Update controls - read input and process
 */
void osci_controls_update( osci_controls_t *osci_controls );

#endif /* _OSCI_CONTROLS_H */

/* vim: set expandtab: */
