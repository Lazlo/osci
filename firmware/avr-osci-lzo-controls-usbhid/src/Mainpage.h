/**
 * \file mainpage.h
 *
 * \mainpage avr-osci-lzo Documentation
 *
 * \section Abstract
 * ... is a "do it yourself" digital storage oscilloscope (DSO) based on a 8-bit RISC microcontroller from Atmels AVR ATmega series.
 *
 * \section Outline
 * This document is intended for developers who want to modify hardware or firmware.\n
 * The details of operating a oscilloscope can be found in "XYZs of Oscilloscopes" at http://www.tek.com/Measurement/App_Notes/XYZs/
 *
 * \ref Firmware
 *
 * \ref Hardware
 *
 * \ref Datasheet
 *
 * \ref bug
 *
 * \ref todo
 */

/**
 * \page Firmware
 *
 * \section OsciFirmwareSources Sources
 * Getting the source from ...
 *
 * \section OsciFirmwareFilesAndDirs Files and Directories
 * - osci.c
 * - osci_config.h
 * - osci_model.h
 * - osci_model.c
 * - osci_controls_config.h
 * - osci_controls.h
 * - osci_controls.c
 * - osci_display.h
 * - osci_display.c
 *
 * - hw/uart.h
 * - hw/uart.c
 * - hw/uart_misc.h
 * - hw/uart_misc.c
 * - hw/timer.h
 * - hw/timer.c
 * - hw/ADC.h
 * - hw/ADC.c
 * - hw/siposhr_config.h
 * - hw/siposhr.h
 * - hw/siposhr.c
 * - hw/pisoshr_config.h
 * - hw/pisoshr.h
 * - hw/pisoshr.c
 *
 * - hw/button_matrix_config.h
 * - hw/button_matrix.h
 * - hw/button_matrix.c
 *
 * - hw/rotary_encoder_config.h
 * - hw/rotary_encoder.h
 * - hw/rotary_encoder.c
 *
 * - hw/lc7981_config.h
 * - hw/lc7981.h
 * - hw/lc7981.c
 * - font5x7.h
 * - graphics.h
 * - graphics.c
 * - strutils.h
 * - strutils.c
 *
 * \section OsciFirmwareComponents Components
 *
 * \subsection OsciFirmwareComponentConfig Configuration
 *
 * \subsection OsciFirmwareComponentDataModel Data Model
 * contains the state of operations
 *
 * \subsection OsciFirmwareComponentControls Controls
 * processing user input
 *
 * \subsection OsciFirmwareComponentDisplay Graphical Output
 * output
 */

/**
 * \page Hardware
 *
 * ... consists of a Atmel AVR ATmega (8-bit RISC) microcontroller clocked at 16MHz, a simple B/W graphical LCD and for now 8 rotary encoders and 25 push buttons
 *
 * \section BillOfMaterial Bill of Material
 *
 * - 1 x LCD DG16080-11
 * - 1 x Inverter (5V DC to 60-100V)
 *
 * \subsection ActiveComponents Active Components
 *
 * - 1 x Atmel AVR ATmega644 (DIP-40)
 * - 1 x 7805 TO-220
 * - 1 x MAX232 (DIP-16)
 * - 1 x 74xx164 (DIP-12)
 * - 3 x 74xx165 (DIP-14)
 *
 * \subsection PassiveComponents Passive Components
 *
 * - 1 x 16MHz HC-49/S Crystal
 * - 2 x 27pF Ceramic Condensators
 * - n x 10K Ohm Resistors
 * - 25 x Diodes
 *
 * \subsection MechanicalComponents Mechanical Components
 *
 * - 1 x Case X x Y x Z
 * - 10 x Push Buttons (7x7x7mm)
 * - 8 x Rotary Encoders
 * - 3 x BNC Connector
 * - 5 x 160 x 100 mm Circuit Boards
 *
 * \section SchematicsAndBoardLayouts Schematics and Board Layouts
 *
 * Checkout the Eagle schematics and PCB layouts for this project at ...
 *
 * \section Casing
 *
 * you can find our DXF drawing at ... (made with QCad)
 */

/**
 * \page Datasheet
 *
 * \section Features
 *
 * - Channels:		2\n
 * - Bandwith:		x MHz\n
 * - Sampling Rate:	x KS/s\n
 * - Precision:		8 Bit\n
 * - Memory:		64 kB\n
 * - Range:		0 V to 5 V\n
 * - Timebase:		x ns/DIV to x s/DIV\n
 * - Freq. Range:	x - x MHz\n
 * - Measure:		dBm, dBV, DC, RMS\n
 * - Interfaces:	USB Device, RS232\n
 * - Display:		160 x 80 Pixel B/W LCD\n
 * - Supply Voltage:	x V
 */
