#include "adc.h"
#include "debug.h"

ADC *ADC_create(void)
{
	DBG("in ADC_create()");
	ADC *adc;
	adc = 0;
	return adc;
}

int ADC_get_state(ADC *adc)
{
	DBG("in ADC_get_state()");
	return 0;
}

int ADC_get_clock(ADC *adc)
{
	DBG("in ADC_get_clock()");
	return 0;
}

void ADC_set_clock(ADC *adc, int clock)
{
	DBG("in ADC_set_clock()");
	return;
}

int ADC_get_channel(ADC *adc)
{
	DBG("in ADC_get_channel()");
	return 0;
}

void ADC_set_channel(ADC *adc, int channel)
{
	DBG("in ADC_set_channel()");
	return;
}

int ADC_free(ADC *adc)
{
	DBG("in ADC_free()");
	return 0;
}
