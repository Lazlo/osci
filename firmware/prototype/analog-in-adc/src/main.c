#include "adc.h"
#include "debug.h"

int
main(int argc, char *argv[])
{
	DBG("in main()");

	ADC *adc;

	adc = ADC_create();
	ADC_get_state(adc);
	ADC_get_clock(adc);
	ADC_set_clock(adc, 0);
	ADC_get_channel(adc);
	ADC_set_channel(adc, 0);
	ADC_free(adc);

	return 0;
}
