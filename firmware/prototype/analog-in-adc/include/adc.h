#ifndef ADC_H
#define ADC_H

typedef struct ADC ADC;

ADC *ADC_create(void);

int ADC_get_state(ADC *adc);

int ADC_get_clock(ADC *adc);

void ADC_set_clock(ADC *adc, int clock);

int ADC_get_channel(ADC *adc);

void ADC_set_channel(ADC *adc, int channel);

int ADC_free(ADC *adc);

#endif /* ADC_H */
