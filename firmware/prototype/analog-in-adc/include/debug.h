#ifndef DEBUG_H
#define DEBUG_H

#include <stdio.h>

#define LOG(level, msg)		printf("%s: %s\n", level, msg)
#define DBG(msg)		LOG("DEBUG", msg)

#endif /* DEBUG_H */
