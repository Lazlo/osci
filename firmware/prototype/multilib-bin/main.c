/**
 * \file main.c
 */

#include <stdio.h>
#include "hwabstr.h"

void
startup(void)
{
	printf("D: in startup()\n");
	return;
}

void
shutdown(void)
{
	printf("D: in shutdown()\n");
	return;
}

int
main(int argc, char *argv[])
{
	printf("D: in main()\n");
	startup();
	hwabstr_init();
	shutdown();
	return 0;
}
