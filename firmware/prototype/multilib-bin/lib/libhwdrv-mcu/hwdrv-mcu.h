/**
 * \file hwdrv-mcu.h
 */

#ifndef HWDRV_MCU_H
#define HWDRV_MCU_H

void
hwdrv_mcu_init(void);

#endif /* HWDRV_MCU_H */
