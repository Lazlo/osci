/**
 * \file hwdrv-pc.h
 */

#ifndef HWDRV_PC_H
#define HWDRV_PC_H

void
hwdrv_pc_init(void);

#endif /* HWDRV_PC_H */
