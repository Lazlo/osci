#include "debug.h"
#include "drawing.h"

///////////////////////////////////////////////////////////////////////////////

int		g_screen_needs_update = 1;

SDL_Surface	*g_screen = 0;


///////////////////////////////////////////////////////////////////////////////

int		display_frame = 1,
		display_graticule = 1,
		display_matrix = 1;

///////////////////////////////////////////////////////////////////////////////

int main( int argc, char **argv );
void setup( void );
void quit( int code );
void loop( void );

void process_events( void );
void handle_key_down( SDL_keysym *keysym );

void draw_screen( void );

///////////////////////////////////////////////////////////////////////////////

int main( int argc, char **argv )
{
	dbg("main( )");
	setup( );
	loop( );
	return 0;
}

///////////////////////////////////////////////////////////////////////////////

void setup( void )
{
	dbg("setup( )");
	if( SDL_Init( SDL_INIT_FLAGS ) == -1 )
	{
		fprintf( stderr, "Could not initialize SDL: %s\n",
			SDL_GetError( ) );
		quit( -1 );
	}

	atexit( SDL_Quit );

	g_screen = SDL_SetVideoMode( SCREEN_WIDTH, SCREEN_HEIGHT, SCREEN_DEPTH,
		SDL_VIDEO_MODE_FLAGS );

	if( g_screen == NULL )
	{
		fprintf( stderr, "Could not set video mode: %s\n",
			SDL_GetError( ) );
		quit( 1 );
	}
#ifdef USE_SDL_TTF
	// Initialize TrueType fonts
	if( TTF_Init() == -1 )
	{
		fprintf( stderr, "Could not initialize SDL TTF: %s\n",
			TTF_GetError( ) );
		quit( 1 );
	}

	atexit( TTF_Quit );
	atexit( free_font );

	//load_font( DEFAULT_FONT_FILE, DEFAULT_FONT_SIZE );
#endif
}

///////////////////////////////////////////////////////////////////////////////

void quit( int code )
{
	dbg("quit( )");
#ifdef USE_SDL_TTF
	TTF_Quit( );
#endif
	SDL_Quit( );

	exit( code );
}

///////////////////////////////////////////////////////////////////////////////

void loop( void )
{
	for( ;; )
	{
		process_events( );
		if( g_screen_needs_update ) {
			g_screen_needs_update = 0;
			draw_screen( );
		}
	}
}

///////////////////////////////////////////////////////////////////////////////

void process_events( void )
{
	SDL_Event event;

	while( SDL_PollEvent( &event ) )
	{
		switch( event.type )
		{
			case SDL_KEYDOWN:
				handle_key_down( &event.key.keysym );
				break;
			case SDL_QUIT:
				quit( 0 );
				break;
		}
	}
}

///////////////////////////////////////////////////////////////////////////////

void handle_key_down( SDL_keysym *keysym )
{
	switch( keysym->sym )
	{
		case SDLK_ESCAPE:
			quit( 0 );
			break;
		case 'q':
			quit( 0 );
			break;
		case SDLK_SPACE:
			dbg( "KEY space was pressed." );
			break;

		case SDLK_UP:
			dbg( "KEY up was pressed." );
			break;
		case SDLK_DOWN:
			dbg( "KEY down was pressed." );
			break;
		case SDLK_LEFT:
			dbg( "KEY left was pressed." );
			break;
		case SDLK_RIGHT:
			dbg( "KEY right was pressed." );
			break;


		// (A)cquire-Menu
		case 'a':
			dbg( "KEY A was pressed." );
			break;
		// (C)ursors-Menu
		case 'c':
			dbg( "KEY C was pressed." );
			break;
		// (D)display-Menu
		case 'd':
			dbg( "KEY D was pressed." );
			break;
		// (H)orizontal-Menu
		case 'h':
			dbg( "KEY H was pressed." );
			break;
		// (M)easure-Menu
		case 'm':
			dbg( "KEY M was pressed." );
			break;
		// (S)torage-Menu (save/recall)
		case 's':
			dbg( "KEY S was pressed." );
			break;
		// (T)rigger-Menu
		case 't':
			dbg( "KEY T was pressed." );
			break;
		// (U)tility-Menu
		case 'u':
			dbg( "KEY U was pressed." );
			break;


		// Channel Menu

		case SDLK_1:
			dbg( "KEY 1 was pressed." );
			break;
		case SDLK_2:
			dbg( "KEY 2 was pressed." );
			break;
		case SDLK_3:
			dbg( "KEY 3 was pressed." );
			break;
		case SDLK_4:
			dbg( "KEY 4 was pressed." );
			break;

		default:
			break;
	}
}

///////////////////////////////////////////////////////////////////////////////

void draw_screen( void )
{
	int i, v;

	// define colors
	Uint32 green;
	green = SDL_MapRGB( g_screen->format, 0x00, 0xFF, 0x00 );


	if( SDL_MUSTLOCK( g_screen ) )
	{
		if( SDL_LockSurface( g_screen ) )
		{
			fprintf( stderr, "Could not lock screen: %s\n",
				SDL_GetError( ) );
			quit( 1 );
		}
	}

	// draw pixel on the center of the screen
	putpixel_scaled( g_screen, 0, 0, green );
	putpixel_scaled( g_screen,
		SCREEN_WIDTH - SCREEN_ZOOM_BY_FACTOR,
		SCREEN_HEIGHT - SCREEN_ZOOM_BY_FACTOR,
		green );
/*
	draw_horizontal_line( g_screen,
		0,
		SCREEN_HEIGHT / 2,
		SCREEN_WIDTH,
		green );

	draw_vertical_line( g_screen,
		SCREEN_WIDTH / 2,
		0,
		SCREEN_HEIGHT,
		green );
*/



	for( i = 0; i < SCREEN_WIDTH / SCREEN_ZOOM_BY_FACTOR; ++i )
		if( i % 10 == 0 )
			putpixel_scaled( g_screen,
				i * SCREEN_ZOOM_BY_FACTOR,
				2 * SCREEN_ZOOM_BY_FACTOR,
				green );

	for( i = 0; i < SCREEN_HEIGHT / SCREEN_ZOOM_BY_FACTOR; ++i )
		if( i % 10 == 0 )
			putpixel_scaled( g_screen,
				2 * SCREEN_ZOOM_BY_FACTOR,
				i * SCREEN_ZOOM_BY_FACTOR,
				green );

	draw_guides( g_screen, 0, 0, green );

	//draw_graticule( g_screen, 0, 0, SCREEN_WIDTH, SCREEN_HEIGHT, green );

	v = SCREEN_HEIGHT / 2;
	for( i = 0; i < SCREEN_WIDTH / SCREEN_ZOOM_BY_FACTOR; ++i ) {
		putpixel_scaled( g_screen,
				i * SCREEN_ZOOM_BY_FACTOR,
				v,
				green );
	}

	if( SDL_MUSTLOCK( g_screen ) )
	{
		SDL_UnlockSurface( g_screen );
	}


	SDL_UpdateRect( g_screen, 0, 0, SCREEN_WIDTH, SCREEN_HEIGHT );
}
