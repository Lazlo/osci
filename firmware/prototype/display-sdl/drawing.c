#include "drawing.h"

void putpixel( SDL_Surface *surface, int x, int y, Uint32 pixel )
{
	int bpp = surface->format->BytesPerPixel;
	Uint8 *p = (Uint8 *)surface->pixels + y * surface->pitch + x * bpp;

	switch( bpp ) {
	case 1:
		*p = pixel;
		break;
	case 2:
		*(Uint16 *)p = pixel;
		break;
	case 3:
		if( SDL_BYTEORDER == SDL_BIG_ENDIAN )
		{
			p[0] = ( pixel >> 16 ) & 0xFF;
			p[1] = ( pixel >> 8 ) & 0xFF;
			p[2] = pixel & 0xFF;
		} else {
			p[0] = pixel & 0xFF;
			p[1] = ( pixel >> 8 ) & 0xFF;
			p[2] = ( pixel >> 16 ) & 0xFF;
		}
		break;
	case 4:
		*(Uint32 *)p = pixel;
		break;
	}
}

///////////////////////////////////////////////////////////////////////////////

void putpixel_scaled( SDL_Surface *surface, int x, int y, Uint32 pixel )
{
	int	i_x,
		i_y;
	for( i_x = 0; i_x < SCREEN_ZOOM_BY_FACTOR; ++i_x )
	{
		for( i_y = 0; i_y < SCREEN_ZOOM_BY_FACTOR; ++i_y )
		{
			putpixel( surface, x + i_x, y + i_y, pixel );
		}
	}
}

///////////////////////////////////////////////////////////////////////////////

void draw_horizontal_line( SDL_Surface *surface, int x, int y, int w, Uint32 pixel )
{
	int i_w;
	for( i_w = 0; i_w < w; i_w += SCREEN_ZOOM_BY_FACTOR )
	{
		putpixel_scaled( surface, x + i_w, y, pixel );
	}
}

///////////////////////////////////////////////////////////////////////////////

void draw_horizontal_dotted_line( SDL_Surface *surface, int x, int y, int w, Uint32 pixel )
{
	int i_w;
	for( i_w = 0; i_w < w; i_w += 2 * SCREEN_ZOOM_BY_FACTOR )
	{
		putpixel_scaled( surface, x + i_w, y, pixel );
	}
}

///////////////////////////////////////////////////////////////////////////////

void draw_vertical_line( SDL_Surface *surface, int x, int y, int h, Uint32 pixel )
{
	int i_h;
	for( i_h = 0; i_h < h; i_h += SCREEN_ZOOM_BY_FACTOR )
	{
		putpixel_scaled( surface, x, y + i_h, pixel );
	}
}

///////////////////////////////////////////////////////////////////////////////

void draw_vertical_dotted_line( SDL_Surface *surface, int x, int y, int h, Uint32 pixel )
{
	int i_h;
	for( i_h = 0; i_h < h; i_h += 2 * SCREEN_ZOOM_BY_FACTOR )
	{
		putpixel_scaled( surface, x, y + i_h, pixel );
	}
}

///////////////////////////////////////////////////////////////////////////////

void draw_rect( SDL_Surface *surface, int x, int y, int w, int h, Uint32 pixel )
{
	draw_vertical_line( surface, x, y, h, pixel );
	draw_vertical_line( surface, x + w, y, h, pixel );
	draw_horizontal_line( surface, x, y, w, pixel );
	draw_horizontal_line( surface, x, y+h, w, pixel );
}

///////////////////////////////////////////////////////////////////////////////

void draw_dotted_rect( SDL_Surface *surface, int x, int y, int w, int h, Uint32 pixel )
{
	draw_vertical_dotted_line( surface, x, y, h, pixel );
	draw_vertical_dotted_line( surface, x + w, y, h, pixel );
	draw_horizontal_dotted_line( surface, x, y, w, pixel );
	draw_horizontal_dotted_line( surface, x, y+h, w, pixel );
}

///////////////////////////////////////////////////////////////////////////////

void draw_cross( SDL_Surface *surface, int x, int y, int w, int h, Uint32 pixel )
{
	draw_horizontal_line( surface,
		x, y + ( h / 2 ), w, pixel );
	draw_vertical_line( surface,
		x + ( w / 2 ), y, h, pixel );
}

///////////////////////////////////////////////////////////////////////////////

void draw_guides( SDL_Surface *surface, int x, int y, Uint32 color )
{
	draw_horizontal_dotted_line( surface,
		x,
		y,
		SCREEN_WIDTH,
		color );
	draw_vertical_dotted_line( surface,
		x,
		y,
		SCREEN_HEIGHT,
		color );
}

///////////////////////////////////////////////////////////////////////////////

void draw_graticule( SDL_Surface *surface, int x, int y, int w, int h, Uint32 color )
{
	int i;
	for( i = 0; i < 8; ++i )
		draw_horizontal_dotted_line( surface,
			10 * SCREEN_ZOOM_BY_FACTOR,
			8 * SCREEN_ZOOM_BY_FACTOR + ( 8 * i ) * SCREEN_ZOOM_BY_FACTOR,
			w - ( 16 * SCREEN_ZOOM_BY_FACTOR ),
			color );

	// 9 vertical deviders (10 cells)

	for( i = 0; i < 10; ++i )
		draw_vertical_dotted_line( surface,
			10 * SCREEN_ZOOM_BY_FACTOR + ( 14 * i ) * SCREEN_ZOOM_BY_FACTOR,
			8 * SCREEN_ZOOM_BY_FACTOR,
			h - ( 16 * SCREEN_ZOOM_BY_FACTOR ),
			color );

	// center lines
	draw_cross( surface,
		10 * SCREEN_ZOOM_BY_FACTOR,
		8 * SCREEN_ZOOM_BY_FACTOR,
		w - ( 20 * SCREEN_ZOOM_BY_FACTOR ),
		h - ( 16 * SCREEN_ZOOM_BY_FACTOR ),
		color );
/*
	draw_horizontal_line( g_screen,
		10 * SCREEN_ZOOM_BY_FACTOR,
		SCREEN_HEIGHT / 2,
		SCREEN_WIDTH - ( 20 * SCREEN_ZOOM_BY_FACTOR ),
		color );

	draw_vertical_line( g_screen,
		SCREEN_WIDTH / 2,
		8 * SCREEN_ZOOM_BY_FACTOR,
		SCREEN_HEIGHT - ( 16 * SCREEN_ZOOM_BY_FACTOR ),
		color );
*/
	// frame
/*
	draw_rect( g_screen,
		10 * SCREEN_ZOOM_BY_FACTOR,			// left padding
		8 * SCREEN_ZOOM_BY_FACTOR,			// upper padding
		SCREEN_WIDTH - 20 * SCREEN_ZOOM_BY_FACTOR,
		SCREEN_HEIGHT - 16 * SCREEN_ZOOM_BY_FACTOR,
		color );
*/
}

///////////////////////////////////////////////////////////////////////////////
#ifdef USE_SDL_TTF
void free_font_glyphs( void )
{
	int i;
	for( i = 0; i < FONT_GLYPHS; i++)
	{
		if( g_font_glyph[i] )
			SDL_FreeSurface( g_font_glyph[i] );
		g_font_glyph[i] = 0;
	}
}

///////////////////////////////////////////////////////////////////////////////

void free_font( void )
{
	if( g_font )
		TTF_CloseFont( g_font );
	g_font = 0;
	free_font_glyphs( );
}

///////////////////////////////////////////////////////////////////////////////

void cache_font_glyphs( void )
{
/*
	SDL_Color fg = { 0, 0, 0, 255};
#if FONT_RENDER_MODE == 1
	SDL_Color bg = { 255, 255, 255, 255 };
#endif
*/
	int i;

	free_font_glyphs( );

	if( !g_font )
		return;
/*
	if( g_font_style != TTF_GetFontStyle( g_font ) )
		TTF_SetFontStyle( g_font, g_font_style );
	if( g_font_kerning != !!TTF_GetFontKerning( g_font ) )
		TTF_SetFontKerning( g_font, g_font_kerning );
	if( g_font_hinting != TTF_GetFontHinting( g_font ) )
		TTF_SetFontHinting( g_font, g_font_hinting );
	if( g_font_outline != TTF_GetFontOutline( g_font ) )
		TTF_SetFontOutline( g_font, g_font_outline );
*/
	for( i = 0; i < FONT_GLYPHS; i++ )
	{
/*
#if FONT_RENDER_MODE == 0
		g_font_glyph[i] = TTF_RenderGlyph_Solid( g_font, i + g_font_start_glyph, fg );
#elif FONT_RENDER_MODE == 1
		g_font_glyph[i] = TTF_RenderGlyph_Shaded( g_font, i + g_font_start_glyph, fg, bg );
#elif FONT_RENDER_MODE == 2
		g_font_glyph[i] = TTF_RenderGlyph_Blended( g_font, i + g_font_start_glyph, fg );
#endif
*/
		if( !g_font_glyph[i] )
		{
			fprintf( stderr, "Could not render glyph for font: %s\n",
				TTF_GetError( ) );
			quit( 4 );
		}

		// cache glyph metrics
/*
		TTF_GlyphMetrics( g_font, i + g_font_start_glyph,
			&g_fgm[i].min_x, &g_fgm[i].max_x,
			&g_fgm[i].min_y, &g_fgm[i].max_y,
			&g_fgm[i].advance );
*/
	}
}

///////////////////////////////////////////////////////////////////////////////

void load_font( char *font_name, int size)
{
	free_font( );
	g_font = TTF_OpenFont( font_name, size );
	if( !g_font )
	{
		fprintf( stderr, "Could not load font: %s\n",
			TTF_GetError( ) );
		quit( 3 );
	}

	printf( "size: %d\n", size );
	printf( "TTF_FontHeight: %d\n", TTF_FontHeight( g_font ) );
	printf( "TTF_FontAscent: %d\n", TTF_FontAscent( g_font ) );
	printf( "TTF_FontDescent: %d\n", TTF_FontDescent( g_font ) );
	printf( "TTF_FontLineSkip: %d\n", TTF_FontLineSkip( g_font ) );
	printf( "TTF_FontFaceIsFixedWidth: %d\n", TTF_FontFaceIsFixedWidth( g_font ) );

	// cache_font_glyphs( );
}

#endif /* USE_SDL_TTF */
