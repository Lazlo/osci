#ifndef _DRAWING_H
#define _DRAWING_H

//#define USE_SDL_TTF

#define SCREEN_ZOOM_BY_FACTOR	4
#define SCREEN_WIDTH		SCREEN_ZOOM_BY_FACTOR * 160
#define SCREEN_HEIGHT		SCREEN_ZOOM_BY_FACTOR * 80
#define SCREEN_DEPTH		8

#ifndef _SDL_H
#include <SDL.h>
#endif /* _SDL_H */

#define SDL_INIT_FLAGS		SDL_INIT_VIDEO
#define SDL_VIDEO_MODE_FLAGS	SDL_SWSURFACE

///////////////////////////////////////////////////////////////////////////////

void putpixel( SDL_Surface *surface, int x, int y, Uint32 pixel );
void putpixel_scaled( SDL_Surface *surface, int x, int y, Uint32 pixel );
void draw_horizontal_line( SDL_Surface *surface, int x, int y, int w, Uint32 pixel );
void draw_horizontal_dotted_line( SDL_Surface *surface, int x, int y, int w, Uint32 pixel );
void draw_vertical_line( SDL_Surface *surface, int x, int y, int h, Uint32 pixel );
void draw_vertical_dotted_line( SDL_Surface *surface, int x, int y, int h, Uint32 pixel );
void draw_rect( SDL_Surface *surface, int x, int y, int w, int h, Uint32 pixel );
void draw_dotted_rect( SDL_Surface *surface, int x, int y, int w, int h, Uint32 pixel );
void draw_cross( SDL_Surface *surface, int x, int y, int w, int h, Uint32 pixel );
void draw_guides( SDL_Surface *surface, int x, int y, Uint32 color );
void draw_graticule( SDL_Surface *surface, int x, int y, int w, int h, Uint32 color );

///////////////////////////////////////////////////////////////////////////////

#ifdef USE_SDL_TTF

#define DEFAULT_FONT_SIZE	7
#define DEFAULT_FONT_FILE	"/usr/share/fonts/truetype/ttf-liberation/LiberationMono-Regular.ttf"

#define FONT_REDER_MODE		2 // 0=solid 1=shaded 2=blended
#define FONT_GLYPHS		128

///////////////////////////////////////////////////////////////////////////////

typedef struct
{
	int	min_x,
		max_x,
		min_y,
		max_y,
		advance;
} GlyphMetrics;

///////////////////////////////////////////////////////////////////////////////

TTF_Font	*g_font = 0;
/*
int		g_font_start_glyph = 0,
		g_font_style = TTF_STYLE_NORMAL,
		g_font_kerning = 1,
		g_font_hinting = TTF_HINTING_NORMAL,
		g_font_outline = 0,
		g_font_size = DEFAULT_FONT_SIZE;
*/
GlyphMetrics	g_fgm[FONT_GLYPHS];
#endif /* USE_SDL_TTF */

///////////////////////////////////////////////////////////////////////////////

#ifdef USE_SDL_TTF
#include <SDL_ttf.h>
#endif /* USE_SDL_TTF */

#ifdef USE_SDL_TTF

SDL_Surface	*g_font_glyph[FONT_GLYPHS];

void free_font_glyphs( void );
void free_font( void );
void cache_font_glyphs( void );
void load_font( char *font_name, int size);
#endif

#endif /* _DRAWING_H */
