#ifndef _DEBUG_H
#define _DEBUG_H

#ifndef _STDIO_H
#include <stdio.h>
#endif

#define dbg(m)	fprintf( stdout, "DEBUG: " m "\n" )

#endif /* _DEBUG_H */
