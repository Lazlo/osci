#include <X11/Xlib.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#define DISPLAY_STRING	":23"
#define WIDTH 360
#define HEIGHT 250

int
main(int argc, char *argv[])
{
	Display *d;
	Window w;
	XEvent e;
	char *msg = "Hello";
	int s;

	d = XOpenDisplay(DISPLAY_STRING);
	if(d == NULL)
	{
		fprintf(stderr, "Cannot open display\n");
		exit(1);
	}

	s = DefaultScreen(d);
	w = XCreateSimpleWindow(d, RootWindow(d, s), 0, 0, WIDTH, HEIGHT, 1, BlackPixel(d, s), WhitePixel(d, s));
	XSelectInput(d, w, ExposureMask | KeyPressMask);
	XMapWindow(d, w);
	while(1)
	{
		XNextEvent(d, &e);
		if(e.type == Expose)
		{
			XFillRectangle(d, w, DefaultGC(d, s), 20, 20, 10, 10);
			XDrawString(d, w, DefaultGC(d, s), 10, 50, msg, strlen(msg));
		}
		if(e.type == KeyPress)
			break;
	}
	XCloseDisplay(d);
	return 0;
}
