#include <check.h>

Suite *firmware_suite(void);
int main(int argc, char *argv[]);

START_TEST(test_firmware_init)
{
	fail_unless(1==1,
		"will never be false");
	fail_unless(1>10,
		"will always be false");
}
END_TEST

Suite *
firmware_suite(void)
{
	Suite *s = suite_create("Firmware");
	TCase *tc_core = tcase_create("Core");

	tcase_add_test(tc_core, test_firmware_init);
	suite_add_tcase(s, tc_core);
	return s;
}

int
main(int argc, char *argv[])
{
	int number_failed;
	Suite *s = firmware_suite();
	SRunner *sr = srunner_create(s);

	srunner_run_all(sr, CK_NORMAL);
	number_failed = srunner_ntests_failed(sr);
	srunner_free(sr);
	return (number_failed == 0) ? 0 : 1;
}
