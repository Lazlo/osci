#include <avr/io.h>
#include <avr/interrupt.h>
#include <util/delay.h> // for _delay_us()
#include <stdio.h> // for sprintf()
#include <string.h> // for strcmp()

/*---------------------------------------------------------------------------*/

// USART related

#define NL "\n\r"

#include "uart.h"

#define puts(s) uart_puts(s)

#define OSCI_MAIN_UART_BAUD 9600

/*---------------------------------------------------------------------------*/

#include "twi_slave.h"

/* TWI Interrupt vector */
/*
SIGNAL(TWI_vect)
{
        // identify current state
        // if state = no info
        //      do nothing
        // if state = SLA+W & ACK
        //      prepare for incoming data
        // if state = DATA ACK
        //      key last = data
        // clear interrupt
        // return
}
*/

/*---------------------------------------------------------------------------*/

#define LED1            6
#define LED1_DDR        DDRE
#define LED1_PORT       PORTE
#define LED1_init       LED1_DDR |= (1<<LED1)
#define LED1_on         LED1_PORT |= (1<<LED1)
#define LED1_off        LED1_PORT &= ~(1<<LED1)
#define LED1_default    LED1_off

/*---------------------------------------------------------------------------*/

#define OSCI_MAIN_TWI_SLA    0x20 // TWI Slave Address of this unit

uint8_t g_controls_key_last = 0;


// Read Controls Task

void controls_init(void)
{
        g_controls_key_last = 0;
}

void controls_update(void)
{
        char key_last_str[3];

        if(twi_slave_called())
        {
                // TODO enable SLA ACK
                if(twi_slave_data_received())
                {
                        // read pressed key (from TWI)
                        g_controls_key_last = twi_slave_read();
                        // TODO enable SLA ACK

                        sprintf( key_last_str, "%d", g_controls_key_last );

                        uart_puts(NL "K");
                        uart_puts(key_last_str);

                        LED1_on;
                        _delay_ms(2);
                        LED1_off;
                        // and signal "controls changed"-event to application
                }
        }
}

/*---------------------------------------------------------------------------*/

// Write Display Task

/*---------------------------------------------------------------------------*/

#define FIRMWARE_BANNER_STR "avr-osci-lzo-main firmware (build using gcc "__VERSION__ " at " __TIMESTAMP__ ")"

int main(void)
{
        LED1_init;
        LED1_default;

        uart_init( OSCI_MAIN_UART_BAUD );

        uart_puts(NL FIRMWARE_BANNER_STR);
        uart_puts(NL "USART ready");

        // initialize TWI interface as slave receiver
        twi_slave_init( OSCI_MAIN_TWI_SLA );
        uart_puts(NL "TWI ready");

        sei();

        for(;;)
        {
                controls_update();
        }
        return 0;
}

/* vim: set expandtab: */
