/**
 * \file osci.c
 */

#include "osci_config.h"

#include <avr/interrupt.h>

#ifdef BUILD_OSCI_CONTROLS_FIRMWARE_WITH_USB
#include <avr/pgmspace.h>
#include <avr/wdt.h>

#include "usbdrv.h"
#include "oddebug.h"
#endif /* BUILD_OSCI_CONTROLS_FIRMWARE_WITH_USB */

#ifdef OSCI_DEBUG_UART
#include "uart.h"

#define OSCI_OUTPUT_STR(s) uart_puts(s)
#define dbg_puts(s) OSCI_OUTPUT_STR(s)
#else
#define dbg_puts(s) // define empty macro when debugging is off
#endif /* OSCI_DEBUG_UART */

#include "hw/timer.h"

#ifdef BUILD_OSCI_CONTROLS_FIRMWARE_WITH_TWI
#include "twi_master.h"
#include <util/twi.h> // for TW_WRITE

#define TWI_BITRATE 400000UL
#define MAIN_UNIT_TWI_SLA 0x20 // slave address of the main unit (where we send our key press events)
#endif /* BUILD_OSCI_CONTROLS_FIRMWARE_WITH_TWI */

#include "osci_controls_config.h"
#include "osci_controls.h"

/* START Controls related fields */
static osci_controls_t g_osci_controls;
/* END Controls related fields */

#define KEY_CHANGED             3
#define KEY_CHANGED_DDR         DDRC
#define KEY_CHANGED_PORT        PORTC
#define KEY_CHANGED_init        KEY_CHANGED_DDR |= 1<<KEY_CHANGED
#define KEY_CHANGED_on          KEY_CHANGED_PORT |= 1<<KEY_CHANGED
#define KEY_CHANGED_off         KEY_CHANGED_PORT &= ~(1<<KEY_CHANGED)
#define KEY_CHANGED_default     KEY_CHANGED_off

#ifdef OSCI_CONTROLS_USE_TIMER
/**
 * \brief timer overflow interrupt handler
 */

OSCI_CONTROLS_ISR_TIMER_OVERFLOW
{
        INTERRUPTS_DISABLE;
        // FIXME this ISR is to long or blocking - move into main loop (where we have more time)
        //osci_controls_update( &g_osci_controls );
        INTERRUPTS_ENABLE;
}
#endif /* OSCI_CONTROLS_USE_TIMER */

/* START USB  */

#ifdef BUILD_OSCI_CONTROLS_FIRMWARE_WITH_USB

#define NUM_KEYS        41

static unsigned char keyPressed(void)
{
#ifndef OSCI_CONTROLS_USE_TIMER
        osci_controls_update( &g_osci_controls );
        return osci_controls_get_key( &g_osci_controls );
#endif /* ! OSCI_CONTROLS_USE_TIMER */
        return 0;
}

static unsigned char reportBuffer[2];
static unsigned char idleRate;

PROGMEM char usbHidReportDescriptor[USB_CFG_HID_REPORT_DESCRIPTOR_LENGTH] = {
        0x05, 0x01,     // USAGE_PAGE (Generic Desktop)
        0x09, 0x06,     // USAGE (Keyboard)
        0xa1, 0x01,     // COLLECTION (Application)
        0x05, 0x07,     //   USAGE_PAGE (Keyboard)
        0x19, 0xe0,     //   USAGE_MINIMUM (Keyboard LeftControl)
        0x29, 0xe7,     //   USAGE_MAXIMUM (Keyboard Right GUI)
        0x15, 0x00,     //   LOGICAL_MINIMUM (0)
        0x25, 0x01,     //   LOGICAL_MAXIMUM (1)
        0x75, 0x01,     //   REPORT_SIZE (1)
        0x95, 0x08,     //   REPORT_COUNT (8)
        0x81, 0x02,     //   INPUT (Data,Var,Abs)
        0x95, 0x01,     //   REPORT_COUNT (1)
        0x75, 0x08,     //   REPORT_SIZE (8)
        0x25, 0x65,     //   LOGICAL_MAXIMUM (101)
        0x19, 0x00,     //   USAGE_MINIMUM (Reserved (no event indicated))
        0x29, 0x65,     //   USAGE_MAXIMUM (Keyboard Application)
        0x81, 0x00,     //   INPUT (Data,Ary,Abs)
        0xc0            // END_COLLECTION
};

/* We use a simplifed keyboard report descriptor which does not support the
 * boot protocol. We don't allow setting status LEDs and we only allow one
 * simultaneous key press (except modifiers). We can therefore use short
 * 2 byte input reports.
 * The report descriptor has been created with usb.org's "HID Descriptor Tool"
 * which can be downloaded from http://www.usb.org/developers/hidpage/.
 * Redundant entries (such as LOGICAL_MINIMUM and USAGE_PAGE) have been omitted
 * for the second INPUT item.
 */

/* Keyboard usage values, see usb.org's HID-usage-tables document, chapter
 * 10 Keyboard/Keypad Page for more codes.
 */
#define MOD_CONTROL_LEFT        (1<<0)
#define MOD_SHIFT_LEFT          (1<<1)
#define MOD_ALT_LEFT            (1<<2)
#define MOD_GUI_LEFT            (1<<3)
#define MOD_CONTROL_RIGHT       (1<<4)
#define MOD_SHIFT_RIGHT         (1<<5)
#define MOD_ALT_RIGHT           (1<<6)
#define MOD_GUI_RIGHT           (1<<7)

#define KEY_A   4
#define KEY_B   5
#define KEY_C   6
#define KEY_D   7
#define KEY_E   8
#define KEY_F   9
#define KEY_G   10
#define KEY_H   11
#define KEY_I   12
#define KEY_J   13
#define KEY_K   14
#define KEY_L   15
#define KEY_M   16
#define KEY_N   17
#define KEY_O   18
#define KEY_P   19
#define KEY_Q   20
#define KEY_R   21
#define KEY_S   22
#define KEY_T   23
#define KEY_U   24
#define KEY_V   25
#define KEY_W   26
#define KEY_X   27
#define KEY_Y   28
#define KEY_Z   29
#define KEY_1   30
#define KEY_2   31
#define KEY_3   32
#define KEY_4   33
#define KEY_5   34
#define KEY_6   35
#define KEY_7   36
#define KEY_8   37
#define KEY_9   38
#define KEY_0   39

#define KEY_F1  58
#define KEY_F2  59
#define KEY_F3  60
#define KEY_F4  61
#define KEY_F5  62
#define KEY_F6  63
#define KEY_F7  64
#define KEY_F8  65
#define KEY_F9  66
#define KEY_F10 67
#define KEY_F11 68
#define KEY_F12 69

static const unsigned char keyReport[NUM_KEYS + 1][2] PROGMEM = {
/* none */      {0, 0},                     /* no key pressed */
/*  1 */        {MOD_SHIFT_LEFT, KEY_A},
/*  2 */        {MOD_SHIFT_LEFT, KEY_B},
/*  3 */        {MOD_SHIFT_LEFT, KEY_C},
/*  4 */        {MOD_SHIFT_LEFT, KEY_D},
/*  5 */        {MOD_SHIFT_LEFT, KEY_E},
/*  6 */        {MOD_SHIFT_LEFT, KEY_F},
/*  7 */        {MOD_SHIFT_LEFT, KEY_G},
/*  8 */        {MOD_SHIFT_LEFT, KEY_H},
/*  9 */        {MOD_SHIFT_LEFT, KEY_I},
/* 10 */        {MOD_SHIFT_LEFT, KEY_J},
/* 11 */        {MOD_SHIFT_LEFT, KEY_K},
/* 12 */        {MOD_SHIFT_LEFT, KEY_L},
/* 13 */        {MOD_SHIFT_LEFT, KEY_M},
/* 14 */        {MOD_SHIFT_LEFT, KEY_N},
/* 15 */        {MOD_SHIFT_LEFT, KEY_O},
/* 16 */        {MOD_SHIFT_LEFT, KEY_P},
/* 17 */        {MOD_SHIFT_LEFT, KEY_Q},
/* 18 */        {MOD_SHIFT_LEFT, KEY_R},
/* 19 */        {MOD_SHIFT_LEFT, KEY_S},
/* 20 */        {MOD_SHIFT_LEFT, KEY_T},
/* 21 */        {MOD_SHIFT_LEFT, KEY_U},
/* 22 */        {MOD_SHIFT_LEFT, KEY_V},
/* 23 */        {MOD_SHIFT_LEFT, KEY_W},
/* 24 */        {MOD_SHIFT_LEFT, KEY_X},
/* 25 */        {MOD_SHIFT_LEFT, KEY_Y},
/* 26 */        {0, KEY_A},
/* 27 */        {0, KEY_B},
/* 28 */        {0, KEY_C},
/* 29 */        {0, KEY_D},
/* 30 */        {0, KEY_E},
/* 31 */        {0, KEY_F},
/* 32 */        {0, KEY_G},
/* 33 */        {0, KEY_H},
/* 34 */        {0, KEY_I},
/* 35 */        {0, KEY_J},
/* 36 */        {0, KEY_K},
/* 37 */        {0, KEY_L},
/* 38 */        {0, KEY_M},
/* 39 */        {0, KEY_N},
/* 40 */        {0, KEY_O},
/* 41 */        {0, KEY_P}
};

static void buildReport(unsigned char key)
{
/* This (not so elegant) cast saves us 10 bytes of program memory */
    *(int *)reportBuffer = pgm_read_word(keyReport[key]);
}

unsigned char usbFunctionSetup(uchar data[8])
{
        usbRequest_t    *rq = (void *)data;

        usbMsgPtr = reportBuffer;

        /* class request type */
        if((rq->bmRequestType & USBRQ_TYPE_MASK) == USBRQ_TYPE_CLASS)
        {
                /* wValue: ReportType (highbyte), ReportID (lowbyte) */
                switch(rq->bRequest) {
                case USBRQ_HID_GET_REPORT:
                        /* we only have one report type, so don't look at wValue */
                        buildReport(keyPressed());
                        return sizeof(reportBuffer);
                case USBRQ_HID_GET_IDLE:
                        usbMsgPtr = &idleRate;
                        return 1;
                case USBRQ_HID_SET_IDLE:
                        idleRate = rq->wValue.bytes[1];
                }
        } else {
                /* no vendor specific requests implemented */
        }
	return 0;
}

void usbHardwareInit(void)
{
        unsigned char i, j;

        // USB
        // reset sequence
        USBDDR |= (1<<USB_CFG_DMINUS_BIT)|(1<<USB_CFG_DPLUS_BIT);   // USB pins as output
        j = 0;
        while(--j) /* USB Reset by device only required on Watchdog Reset */
        {
                i = 0;
                while(--i); /* delay >10ms for USB reset */
        }
        // remove USB reset condition
        USBDDR &= ~((1<<USB_CFG_DMINUS_BIT)|(1<<USB_CFG_DPLUS_BIT));   // USB pins as input
}

#endif /* BUILD_OSCI_CONTROLS_FIRMWARE_WITH_USB */

/* END USB */

/**
 * \brief initialize everything
 */
void osci_init( void )
{
        /* setup "key changed" pin as output - we use this pin to communicate
         * a "please read me" event to the micro controller that is supposed
         * to read the last key pressed by this device */
        KEY_CHANGED_init;
        KEY_CHANGED_default;

        #ifdef OSCI_DEBUG_UART
        /* setup uart (for debugging) */
        uart_init(OSCI_DEBUG_UART_BAUD);                        // UART
        dbg_puts( "\r\n" );
        dbg_puts( "avr-osci-lzo-v1 (build " __DATE__ " " __TIME__ ")\r\n" );
        dbg_puts( "UART ready!\r\n" );
        #endif /* OSCI_DEBUG_UART */


        #ifdef BUILD_OSCI_CONTROLS_FIRMWARE_WITH_USB
        /* setup usb */
        usbHardwareInit();                                      // USB
        /* configure timer 0 for a rate of 12M/(1024 * 256) = 45.78 Hz (~22ms) */
        TCCR0 = 5; /* timer 0 prescaler: 1024 */
        wdt_enable(WDTO_2S);
        usbInit();
        #endif /* BUILD_OSCI_CONTROLS_FIRMWARE_WITH_USB */

        /* START Controls init */

        osci_controls_init( &g_osci_controls );                 // Controls

        #ifdef BUILD_OSCI_CONTROLS_FIRMWARE_WITH_TWI
        /* setup twi as master transmitter */
        twi_master_init( TWI_BITRATE );                         // TWI
        #endif /* BUILD_OSCI_CONTROLS_FIRMWARE_WITH_TWI */

        #ifdef OSCI_CONTROLS_USE_TIMER
        timer_init( OSCI_CONTROLS_TIMER );
        // set operation mode
        // overflow interrupt enable
        // select clock source (internal/prescaler/external)
        #endif /* OSCI_CONTROLS_USE_TIMER */

        dbg_puts( "OSCI CONTROLS ready!\r\n" );

        /* END Controls init */
}

/**
 * \brief loop forever
 */
void osci_loop( void )
{
#ifdef BUILD_OSCI_CONTROLS_FIRMWARE_WITH_USB
        uint8_t key, lastKey, keyDidChange = 0;
        uint8_t idleCounter = 0;
#endif /* BUILD_OSCI_CONTROLS_FIRMWARE_WITH_USB */

        INTERRUPTS_ENABLE;
        for( ;; )
        {
#ifdef BUILD_OSCI_CONTROLS_FIRMWARE_WITH_USB
                wdt_reset();
                usbPoll();

                key = keyPressed();
                if(lastKey != key)
                {
                        KEY_CHANGED_on;
                        lastKey = key;
                        keyDidChange = 1;
/*
                        twi_master_start();
                        twi_master_write_header( MAIN_UNIT_TWI_SLA+TW_WRITE );
                        twi_master_write_data( lastKey );
                        twi_master_stop();
*/
                }

                if(TIFR & (1<<TOV0))                    /* 22 ms timer */
                {
                        TIFR = 1<<TOV0;
                        if(idleRate != 0)
                        {
                                if(idleCounter > 4) {
                                        idleCounter -=5; /* 22 ms in units of 4 ms */
                                } else {
                                        idleCounter = idleRate;
                                        keyDidChange = 1;
                                }
                        }
                }

                if(keyDidChange && usbInterruptIsReady())
                {
                        keyDidChange = 0;
                        buildReport(lastKey);
                        usbSetInterrupt(reportBuffer, sizeof(reportBuffer));
                }

                KEY_CHANGED_off;
#endif /* BUILD_OSCI_CONTROLS_FIRMWARE_WITH_USB */
        }
}

/**
 * \brief point of entry
 */
int main( void )
{
        osci_init( );
        osci_loop( );
        return 0;
}

/* vim: set expandtab: */
