/**
 * \file osci_controls.c
 */

#include "osci_config.h" // for OSCI_DEBUG_UART
#include "osci_controls_config.h"
#include "osci_controls.h"

#include "hw/siposhr.h"
#include "hw/pisoshr.h"

#ifdef OSCI_DEBUG_UART
#include "uart.h"

#define OC_OUTPUT_STR(s) uart_puts(s)
#define dbg_puts(s) OC_OUTPUT_STR(s)

#include "hw/uart_misc.h"
#include "strutils.h"
#endif /* OSCI_DEBUG_UART */

#include "hw/button_matrix.h"
#include "hw/rotary_encoder.h"

static struct siposhr_t g_siposhr;
static struct pisoshr_t g_pisoshr;

/*
uint8_t rotary_enc_get_knob( uint8_t i )
{
        // FIXME: warning: ‘re_id’ may be used uninitialized in this function
        uint8_t re_id;
        switch( i ) {
        case RE1_A:
        case RE1_B: re_id = RE1;
                break;
        case RE2_A:
        case RE2_B: re_id = RE2;
                break;
        case RE3_A:
        case RE3_B: re_id = RE3;
                break;
        case RE4_A:
        case RE4_B: re_id = RE4;
                break;
        case RE5_A:
        case RE5_B: re_id = RE5;
                break;
        case RE6_A:
        case RE6_B: re_id = RE6;
                break;
        case RE7_A:
        case RE7_B: re_id = RE7;
                break;
        case RE8_A:
        case RE8_B: re_id = RE8;
                break;
        }
        return re_id;
}

char *rotary_enc_get_knob_name( uint8_t re_id )
{
        switch( re_id ) {
        case RE1:               return RE1_NAME;
        case RE2:               return RE2_NAME;
        case RE3:               return RE3_NAME;
        case RE4:               return RE4_NAME;
        case RE5:               return RE5_NAME;
        case RE6:               return RE6_NAME;
        case RE7:               return RE7_NAME;
        case RE8:               return RE8_NAME;
        default: return "";
        }
}
*/

unsigned char osci_controls_get_key( osci_controls_t *osci_controls )
{
        unsigned char key = 0;
        // any key in button matrix changed?
        if(osci_controls->bm.changed) {
                osci_controls->bm.changed = 0;
                key = osci_controls->bm.lastKey;
                osci_controls->bm.lastKey = 0;
        }
        // any changes in rotary encoder states?
        if(osci_controls->re.changed) {
                osci_controls->re.changed = 0;
                key = 25+1+osci_controls->re.lastKey; // +25 for the 25 push buttons, +1 for key offset
                osci_controls->re.lastKey = 0;
        }
        return key;
}

void osci_controls_init( osci_controls_t *osci_controls )
{
/*
        // populate siposhr_t with GpioPin instances
        *g_siposhr.cp = GpioPin_create( SIPOSHR_CP, &SIPOSHR_CP_DDR, &SIPOSHR_CP_PORT, &SIPOSHR_CP_PIN );
        *g_siposhr.mr = GpioPin_create( SIPOSHR_MR, &SIPOSHR_MR_DDR, &SIPOSHR_MR_PORT, &SIPOSHR_MR_PIN );
        *g_siposhr.ds1 = GpioPin_create( SIPOSHR_DS1, &SIPOSHR_DS1_DDR, &SIPOSHR_DS1_PORT, &SIPOSHR_DS1_PIN );
        // configure gpio directions
        GpioPin_asOutput( &g_siposhr.cp );
        GpioPin_asOutput( &g_siposhr.mr );
        GpioPin_asOutput( &g_siposhr.ds1 );
*/
        siposhr_init( &g_siposhr );		// serial in / parallel out shift register
        pisoshr_init( &g_pisoshr );		// parallel in / serial out shift register
        button_matrix_init( &osci_controls->bm );
        re_init( &osci_controls->re );
}

void osci_controls_update( osci_controls_t *osci_controls )
{
        uint8_t         i,                      // temporary counter variable
                        bm_col,                 // counter for button matrix columns
                        bytes_read[PISOSHR_READ_BYTES]; // buffer for serially read bytes from the 74xx165

        re_data_t       re_data;

#ifdef DEBUG_OSCI_CONTROLS_SERIAL_DATA
        char            str_bm_col[4];
#endif /* DEBUG_OSCI_CONTROLS_SERIAL_DATA */

        // write serial data
        siposhr_reset( &g_siposhr );
        // set first column high, all others low
        siposhr_write( &g_siposhr, 1 );

        // read serial data
        for( bm_col = 0; bm_col < BUTTON_MATRIX_COLS; bm_col++ ) {

                // initialize space for serialy read bytes
                for( i = 0; i < PISOSHR_READ_BYTES; i++ ) {
                        bytes_read[i] = 0;
                }

                // read from 74xx165
                pisoshr_load( &g_pisoshr );
                for( i = 0; i < PISOSHR_READ_BYTES; i++ ) {
                        bytes_read[i] = pisoshr_read_byte( &g_pisoshr );
                }

                button_matrix_update( &osci_controls->bm, bm_col, bytes_read[PISOSHR_BUTTON_MATRIX_BYTE] );

                re_data = bytes_read[PISOSHR_ROTARY_BYTE_LOW];
                re_data += ( bytes_read[PISOSHR_ROTARY_BYTE_HIGH] << 8 );

                re_update( &osci_controls->re, re_data );

#if defined( DEBUG_OSCI_CONTROLS_SERIAL_DATA ) && defined( OSCI_DEBUG_UART )
                itoa( 1+bm_col, str_bm_col, 10 );
                // output result...
                dbg_puts( "READ: column " );
                dbg_puts( str_bm_col );
                dbg_puts( " data: " );

                for( i = 0; i < PISOSHR_READ_BYTES; i++ ) {
                        uart_puts_byte( bytes_read[i] );
                        dbg_puts( " " );
                }
                dbg_puts( "\r\n" );
#endif /* defined( DEBUG_OSCI_CONTROLS_SERIAL_DATA ) && defined( OSCI_DEBUG_UART ) */

                // shift / pull next column high, all others low
                siposhr_shift( &g_siposhr );
        }
#if defined( DEBUG_OSCI_CONTROLS_SERIAL_DATA ) && defined( OSCI_DEBUG_UART )
        dbg_puts( "\r\n" );

        for( i = 0; i < BUTTON_MATRIX_COLS; i++ ) {
                itoa( i, str_bm_col, 10 );
                dbg_puts( "INFO: osci_controls->bm[" );
                dbg_puts( str_bm_col );
                dbg_puts(  "] = " );
                uart_puts_byte( osci_controls->bm[i] );
                dbg_puts( "\r\n" );
        }
        dbg_puts( "\r\n" );
#endif /* defined( DEBUG_OSCI_CONTROLS_SERIAL_DATA ) && defined( OSCI_DEBUG_UART ) */
        return;
}

/* vim: set expandtab: */
