/**
 * \file uart_misc.h
 */

#ifndef _UART_MISC_H
#define _UART_MISC_H

#include <inttypes.h>

/**
 * \brief output byte as ASCII over UART
 */
void uart_puts_byte( uint8_t byte );

#endif /* _UART_MISC_H */
