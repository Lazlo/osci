#include "GpioPin.h"

GpioPin_t
GpioPin_create(unsigned char offset, volatile uint8_t *ddr, volatile uint8_t *port, volatile uint8_t *pin) {
	GpioPin_t p = { offset, ddr, port, pin };
	return p;
}

unsigned char
GpioPin_getDirection(GpioPin_t *p)
{
	return (*p->ptr_ddr & (1<<p->offset) ? 1 : 0);
}

void
GpioPin_asOutput(GpioPin_t *p)
{
        *p->ptr_ddr |= (1 << p->offset);
}

void
GpioPin_asInput(GpioPin_t *p)
{
        *p->ptr_ddr &= ~(1 << p->offset);
}

void
GpioPin_writeBit(GpioPin_t *p, unsigned char bit)
{
        if(bit)
                *p->ptr_port |= (1 << p->offset);
        else
                *p->ptr_port &= ~(1 << p->offset);
}

unsigned char
GpioPin_readBit(GpioPin_t *p)
{
        return (*p->ptr_pin & (1 << p->offset) ? 1 : 0);
}

/* vim: set expandtab: */
