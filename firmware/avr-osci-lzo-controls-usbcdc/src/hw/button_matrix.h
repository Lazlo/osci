/**
 * \file button_matrix.h
 */

#ifndef _BUTTON_MATRIX_H
#define _BUTTON_MATRIX_H

#include "button_matrix_config.h"		// for BUTTON_MATRIX_COLS
#include <inttypes.h>				// for uint8_t

/*! \brief button pressed event id */
#define BUTTON_MATRIX_EVENT_KEY_PRESSED		1
/*! \brief button released event id */
#define BUTTON_MATRIX_EVENT_KEY_RELEASED	0

/*! \brief event id */
typedef uint8_t bm_event_t;
/*! \brief key id */
typedef uint8_t bm_key_id_t;
/*! \brief base type for row and col data types */
typedef uint8_t bm_data_t;

/*! \brief button matrix instance structure */
typedef struct {
        /*! \brief last state for all button matrix keys */
        bm_data_t       last[BUTTON_MATRIX_COLS];
        unsigned char   lastKey, changed;
} bm_t;

/**
 * \brief resolve column and row into the key id
 */
bm_key_id_t button_matrix_get_key( uint8_t column, uint8_t row );

#ifdef BUTTON_MATRIX_INCLUDE_BUTTON_NAMES
/**
 * \brief resolve key id into a its name (char string)
 */
char *button_matrix_get_key_name( bm_key_id_t key_id );
#endif /* BUTTON_MATRIX_INCLUDE_BUTTON_NAMES */

/**
 * \brief handler for key changes
 */
void button_matrix_handle_key_change( uint8_t column, uint8_t row, bm_event_t bm_event );

/**
 * \brief initialize datastructures
 */
void button_matrix_init( bm_t *bm );

/**
 * \brief update / do the work
 */
void button_matrix_update( bm_t *bm, uint8_t column, uint8_t column_data );

#endif /* _BUTTON_MATRIX_H */

/* vim: set expandtab: */
