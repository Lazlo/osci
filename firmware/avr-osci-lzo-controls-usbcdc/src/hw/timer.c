/**
 \file timer.c
 */

#include "timer.h"

void timer_set_top( unsigned char timer, uint8_t top )
{
        // set TOP in Output Compare Register
        #if defined(__AVR_ATmega644__)
        OUTPUT_COMPARE_REGISTER_A = top;
        #endif /* defined(__AVR_ATmega644__) */
        #if defined(__AVR_ATmega32__) | defined(__AVR_ATmega16__)
        OUTPUT_COMPARE_REGISTER = top;
        #endif /* defined(__AVR_ATmega32__) | defined(__AVR_ATmega16__) */
}

void timer_set_wave_generation_mode( unsigned char timer, int wgm )
{
        /* set Wave Generation mode */
        /* - Normal mode
         * - Clear Timer on Compare Match (CTC) mode
         * - Fast-PWM mode
         */

        #if defined(__AVR_ATmega644__)
        // TIMER_COUNTER_CONTROL_REGISTER_A = WGM01 WGM00
        // TIMER_COUNTER_CONTROL_REGISTER_B = WGM02
        #endif /* defined(__AVR_ATmega644__) */
        #if defined(__AVR_ATmega32__) | defined(__AVR_ATmega16__)
        //TIMER_COUNTER_CONTROL_REGISTER = WGM...
        switch( wgm ) {
        case TIMER_COUNTER_NORMAL_MODE:
                TIMER_COUNTER_CONTROL_REGISTER &= ~( ( 1 << TIMER_WAVEFORM_GENERATOR_MODE_BIT1 ) | ( 1 << TIMER_WAVEFORM_GENERATOR_MODE_BIT0 ) ); break;
        case TIMER_COUNTER_PHASE_CORRECT_PWM_MODE:
                TIMER_COUNTER_CONTROL_REGISTER |= ( 1 << TIMER_WAVEFORM_GENERATOR_MODE_BIT0 ); break;
        case TIMER_COUNTER_CTC_MODE:
                TIMER_COUNTER_CONTROL_REGISTER |= ( 1 << TIMER_WAVEFORM_GENERATOR_MODE_BIT1 ); break;
        case TIMER_COUNTER_FAST_PWM_MODE:
                TIMER_COUNTER_CONTROL_REGISTER |= ( 1 << TIMER_WAVEFORM_GENERATOR_MODE_BIT1 ) | ( 1 << TIMER_WAVEFORM_GENERATOR_MODE_BIT0 ); break;
        }
        #endif /* defined(__AVR_ATmega32__) | defined(__AVR_ATmega16__) */
}

void timer_set_output_compare_mode( unsigned char timer )
{
        /* set Output Compare mode  */
        /*
         * in normal mode: ?
         * in CTC mode: set | clear | toggle OC0 pin
         * in Fast-PWM mode: inverted / non-inverted PWM
         * in Phase Correct PWM mode: inverted / non-inverted PWM
         */
        #if defined(__AVR_ATmega644__)
        TIMER_COUNTER_CONTROL_REGISTER_A |= ( 1 << TIMER_COMPARE_MATCH_OUTPUT_A_MODE_BIT0 ); // Toggle OC0A on Compare Match (non-PWM mode)
        #endif /* defined(__AVR_ATmega644__) */
        #if defined(__AVR_ATmega32__) | defined(__AVR_ATmega16__)
        TIMER_COUNTER_CONTROL_REGISTER |=  ( 1 << TIMER_COMPARE_MATCH_OUTPUT_MODE_BIT0 ); // Toggle OC0 on Compare Match (non-PWM mode)
        #endif /* defined(__AVR_ATmega32__) | defined(__AVR_ATmega16__) */
}

void timer_set_clock_source( unsigned char timer, int cs )
{
        volatile uint8_t *ptr_tccr;
        uint8_t tccr_val;

        switch(timer) {
        case 0:
                // get address of timer counter control register
#if defined(__AVR_ATmega644__)
                ptr_tccr = &TIMER_COUNTER_CONTROL_REGISTER_B;
#elif defined(__AVR_ATmega32__) | defined(__AVR_ATmega16__) | defined(__AVR_ATmega8__)
                ptr_tccr = &TIMER_COUNTER_CONTROL_REGISTER;
#else
#error __FILE__ ":" __LINE__ "no match for MCU."
#endif
        break;
        case 2:
#if defined(__AVR_ATmega8__)
                ptr_tccr = &TIMER2_COUNTER_CONTROL_REGISTER;
#else
#error __FILE__ ":" __LINE__ "no match for MCU."
#endif
        break;
        default:
                // ERROR no such timer
                return;
        }

        // copy value from timer counter control register
        tccr_val = *ptr_tccr;

        /* set Clock Source */
        /*
         * no clock source - inactive
         * internal clock source
         * prescaler (1, 8, 64, 256, 1024)
         * external clock source (T0 pin)
         */
        switch( cs ) {
        // (0) 0 0 0 - No clock source
        case TIMER_CLOCK_STOP:
                tccr_val &= ~( ( 1 << TIMER_CLOCK_SELECT_BIT2 ) | ( 1 << TIMER_CLOCK_SELECT_BIT1 ) | ( 1 << TIMER_CLOCK_SELECT_BIT0 ) ); break;
        // (1) 0 0 1 - clk_io / 1 (no prescaling)
        case TIMER_CLOCK_DIV_1:
                tccr_val |= ( 1 << TIMER_CLOCK_SELECT_BIT0 ); break;
        // (2) 0 1 0 - clk_io / 8
        case TIMER_CLOCK_DIV_8:
                tccr_val |= ( 1 << TIMER_CLOCK_SELECT_BIT1 ); break;
        // (3) 0 1 1 - clk_io / 64
        case TIMER_CLOCK_DIV_64:
                tccr_val |= ( 1 << TIMER_CLOCK_SELECT_BIT1 ) | ( 1 << TIMER_CLOCK_SELECT_BIT0 ); break;
        // (4) 1 0 0 - clk_io / 256
        case TIMER_CLOCK_DIV_256:
                tccr_val |= ( 1 << TIMER_CLOCK_SELECT_BIT2 ); break;
        // (5) 1 0 1 - clk_io / 1024
        case TIMER_CLOCK_DIV_1024:
                tccr_val |= ( 1 << TIMER_CLOCK_SELECT_BIT2 ) | ( 1 << TIMER_CLOCK_SELECT_BIT0 ); break;
        // (6) 1 1 0 - External clock source on T0 pin. Clock on falling edge.
        case TIMER_CLOCK_T0_FALLING:
                tccr_val |= ( 1 << TIMER_CLOCK_SELECT_BIT2 ) | ( 1 << TIMER_CLOCK_SELECT_BIT1 ); break;
        // (7) 1 1 1 - External clock source on T0 pin. Clock on rising edge.
        case TIMER_CLOCK_T0_RISING:
                tccr_val |= ( 1 << TIMER_CLOCK_SELECT_BIT2 ) | ( 1 << TIMER_CLOCK_SELECT_BIT1 ) | ( 1 << TIMER_CLOCK_SELECT_BIT0 ); break;
        }
        // write new value back to timer counter control register
        *ptr_tccr = tccr_val;
}

void timer_interrupt_enable( unsigned char timer )
{
        switch(timer) {
        case 0:
                TIMER_COUNTER_INTERRUPT_MASK_REGISTER |= ( 1 << TIMER_COUNTER_OVERFLOW_INTERRUPT_ENABLE );
        break;
        case 2:
                TIMER2_COUNTER_INTERRUPT_MASK_REGISTER |= ( 1 << TIMER2_COUNTER_OVERFLOW_INTERRUPT_ENABLE );
        break;
        default:
                return;
        }
}

void timer_init( unsigned char timer )
{
//      timer_set_top( 128 );
//      timer_set_wave_generation_mode( TIMER_COUNTER_NORMAL_MODE );
//      timer_set_output_compare_mode( );

        // select clock source
        timer_set_clock_source( timer, TIMER_CLOCK_DIV_1024 );

        // setup OC0 pin as output
        #if defined(__AVR_ATmega644__)
//      OUTPUT_COMPARE_1A_DDR |= ( 1 << OUTPUT_COMPARE_1A );
        #endif /* defined(__AVR_ATmega644__) */

        #if defined(__AVR_ATmega32__) | defined(__AVR_ATmega16__)
//      OUTPUT_COMPARE_0_DDR |= ( 1 << OUTPUT_COMPARE_0 );
        #endif /* defined(__AVR_ATmega32__) | defined(__AVR_ATmega16__) */

        // enable overflow interrupt
        timer_interrupt_enable( timer );
}

/* vim: set expandtab: */
