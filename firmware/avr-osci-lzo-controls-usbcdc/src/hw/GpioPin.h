#ifndef GPIO_PIN_H
#define GPIO_PIN_H

#include "stdint.h" // for uint8_t

#define GPIOPIN_DIRECTION_INPUT 0
#define GPIOPIN_DIRECTION_OUTPUT 1

struct GpioPin {
        unsigned char offset;
        volatile uint8_t *ptr_ddr, *ptr_port, *ptr_pin;
};

typedef struct GpioPin GpioPin_t;

GpioPin_t
GpioPin_create(unsigned char offset, volatile uint8_t *ddr, volatile uint8_t *port, volatile uint8_t *pin);

unsigned char
GpioPin_getDirection(GpioPin_t *p);

void
GpioPin_asOutput(GpioPin_t *p);

void
GpioPin_asInput(GpioPin_t *p);

void
GpioPin_writeBit(GpioPin_t *p, unsigned char bit);

unsigned char
GpioPin_readBit(GpioPin_t *p);

#endif /* GPIO_PIN_H */

/* vim: set expandtab: */
