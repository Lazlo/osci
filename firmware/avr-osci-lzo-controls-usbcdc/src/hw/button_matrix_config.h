/**
 * \file button_matrix_config.h
 */

#ifndef _BUTTON_MATRIX_CONFIG_H
#define _BUTTON_MATRIX_CONFIG_H

/* Button Matrix rows and columns */

/*! \brief number of columns */
#define BUTTON_MATRIX_COLS	5
/*! \brief number of rows */
#define BUTTON_MATRIX_ROWS	5

/*! \brief #define BUTTON_MATRIX_KEY_PRESS_ONLY will disable detection of key release events */
#define BUTTON_MATRIX_KEY_PRESS_ONLY

/* Debugging */

/**
 * \brief enable debugging by defining DEBUG_BUTTON_MATRIX
 */
#define DEBUG_BUTTON_MATRIX

/**
 * \brief by defining BUTTON_MATRIX_INCLUDE_BUTTON_NAMES string names for button matrix keys will be
 * compiled into the firmware and be written to debug output.
 */
//#define BUTTON_MATRIX_INCLUDE_BUTTON_NAMES

/**
 * \brief by defining BUTTON_MATRIX_INCLUDE_COL_ROW_PAIR the col/row pair of a key will be printed to
 * debug output
 */
//#define BUTTON_MATRIX_INCLUDE_COL_ROW_PAIR

/**
 * \brief enable debugging of calls to button_matrix_update by defining DEBUG_BUTTON_MATRIX_TICKS
 */
//#define DEBUG_BUTTON_MATRIX_TICKS

/* Buttons */
#define KEY_UNKNOWN		0

/*! \brief Save / Recall Menu Button */
#define KEY_SAVE_RECALL		1	// S1
/*! \brief Utility Menu Button */
#define KEY_UTILITY		6
/*! \brief Channel 1 Menu Button */
#define KEY_CH1_MENU		11
/*! \brief REF Menu Button */
#define KEY_REF			16
/*! \brief Channel 1 V/DIV Button (inside Rotary Encoder) */
#define KEY_CH1_VDIV		21

/*! \brief Measure Menu Button */
#define KEY_MEASURE		2
/*! \brief Cursor Menu Button */
#define KEY_CURSOR		7
/*! \brief Math Menu Button */
#define KEY_MATH		12
/*! \brief Force Trigger Button */
#define KEY_FORCE_TRIGGER	17
/*! \brief Channel 1 Position Button (inside Rotary Encoder) */
#define KEY_CH1_POSITION	22

/*! \brief Acquire Menu Button */
#define KEY_ACQUIRE		3
/*! \brief Display Menu Button */
#define KEY_DISPLAY		8
/*! \brief Channel 2 Menu Button */
#define KEY_CH2_MENU		13
/*! \brief Channel 2 V/DIV Button (inside Rotary Encoder) */
#define KEY_CH2_VDIV		18
/*! \brief Channel 2 Position Button (inside Rotary Encoder) */
#define KEY_CH2_POSITION	23

/*! \brief Default Setup Button */
#define KEY_DEFAULT_SETUP	4
/*! \brief Help Button */
#define KEY_HELP		9
/*! \brief Horizontal Menu Button */
#define KEY_HORIZONTAL_MENU	14
/*! \brief Horizontal S/DIV Button (inside Rotary Encoder) */
#define KEY_HORIZONTAL_SDIV	19
/*! \brief Horizontal Position Button (inside Rotary Encoder) */
#define KEY_HORIZONTAL_POSITION	24

/*! \brief Single Shot Button */
#define KEY_SINGLE		5
/*! \brief Run / Stop Button */
#define KEY_RUN_STOP		10
/*! \brief Trigger Menu Button */
#define KEY_TRIGGER_MENU	15
/*! \brief Trigger Level Button (inside Rotary Encoder) */
#define KEY_TRIGGER_LEVEL	20
/*! \brief General Purpose Button (inside Rotary Encoder) */
#define KEY_GENERAL_PURPOSE	25

#endif /* _BUTTON_MATRIX_CONFIG_H */

/* vim: set expandtab: */
