/**
 \file rotary_encoder.c
 */

#include "rotary_encoder_config.h"
#include "rotary_encoder.h"

#if defined( RE_DEBUG_USING_UART ) || defined( RE_DEBUG_USING_UART_VERBOSE )
#include "uart.h"

#define RE_OUTPUT_STR(s) uart_puts(s)
#define dbg_puts(s) RE_OUTPUT_STR(s)

#include "uart_misc.h"
#include "strutils.h"
#endif /* defined( RE_DEBUG_USING_UART ) || defined( RE_DEBUG_USING_UART_VERBOSE ) */

#ifdef RE_DEBUG_USING_LEDS
void debug_byte_to_leds( const uint8_t byte )
{
        if( byte & ( 1 <<  0 ) )        LED2_PORT |= ( 1 << LED2 );
        else                            LED2_PORT &= ~( 1 << LED2 );
        if( byte & ( 1 << 1 ) )         LED3_PORT |= ( 1 << LED3 );
        else                            LED3_PORT &= ~( 1 << LED3 );
        if( byte & ( 1 <<  2 ) )        LED4_PORT |= ( 1 << LED4 );
        else                            LED4_PORT &= ~( 1 << LED4 );
        if( byte & ( 1 << 3 ) )         LED5_PORT |= ( 1 << LED5 );
        else                            LED5_PORT &= ~( 1 << LED5 );
}
#endif /* RE_DEBUG_USING_LEDS */

#if defined( RE_DEBUG_USING_UART ) || defined( RE_DEBUG_USING_UART_VERBOSE )
void debug_re_to_uart( const re_t *re, const re_id_t re_id, const re_direction_t re_direction )
{
        char            str_re_id[2];		// character string for the numeric rotary encoder id (one digit+termination)

        itoa( 1+re_id, str_re_id, 10 );

        dbg_puts( "D: R" );
        dbg_puts( str_re_id );

        #ifdef RE_DEBUG_USING_UART_VERBOSE
        dbg_puts( " - (tmp)re->data: " );

        #if RE_DATA_INTTYPE_WIDTH == 8
        uart_puts_byte( re->data );
        #elif RE_DATA_INTTYPE_WIDTH == 16
        uart_puts_byte( (uint8_t) ( re->data >> 8 ) );          // high byte
        uart_puts_byte( (uint8_t) re->data );                   // low byte
        #else
        #error "RE_DATA_INTTYPE_WIDTH is neither 8 nor 16"
        #endif /* RE_DATA_INTTYPE_WIDTH */

        dbg_puts( " (tmp)re->data_last: " );
        #if RE_DATA_INTTYPE_WIDTH == 8
        uart_puts_byte( re->data_last );
        #elif RE_DATA_INTTYPE_WIDTH == 16
        uart_puts_byte( (uint8_t) ( re->data_last >> 8 ) );     // high byte
        uart_puts_byte( (uint8_t) re->data_last );              // low byte
        #else
        #error "RE_DATA_INTTYPE_WIDTH is neither 8 nor 16"
        #endif /* RE_DATA_INTTYPE_WIDTH */

        #endif /* RE_DEBUG_USING_UART_VERBOSE */

        if( re_direction == RE_TURNED_CW )
                dbg_puts( " - CW  " );
        else if( re_direction == RE_TURNED_CCW )
                dbg_puts( " - CCW " );
        else
                dbg_puts( " - ?   " );

        dbg_puts( "\r\n" );
}
#endif /* defined( RE_DEBUG_USING_UART ) || defined( RE_DEBUG_USING_UART_VERBOSE ) */

void handle_re_turned_cw( re_t *re, const re_id_t re_id ) { }

void handle_re_turned_ccw( re_t *re, const re_id_t re_id ) { }

#if defined( RE_IO_TYPE_PARALLEL )
re_data_t re_read_all( void )
{
        re_data_t	data_read;

	// FIXME initialize data_read correspondig to the active-high or active-low
	// configuration for each encoder

        data_read = 0xff;

        if( RE1_A_READ )        data_read |= ( 1 << 0 );
        else                    data_read &= ~( 1 << 0 );
        if( RE1_B_READ )        data_read |= ( 1 << 1 );
        else                    data_read &= ~( 1 << 1 );
        if( RE2_A_READ )        data_read |= ( 1 << 2 );
        else                    data_read &= ~( 1 << 2 );
        if( RE2_B_READ )        data_read |= ( 1 << 3 );
        else                    data_read &= ~( 1 << 3 );

        return data_read;
}
#endif /* defined( RE_IO_TYPE_PARALLEL ) */

/**
 \bug some how in serial mode the directions have fliped : (
 */
re_direction_t re_tell_direction( const re_t *re, const re_id_t re_id )
{
        re_direction_t  re_direction = 0;
	// FIXME: make initialization depend on active-low or active-high configuration
        re_data_t       re_data_last = 0,
                        re_data = 0,
                        tmp_data_last,
                        tmp_data;
        #if defined( RE_DEBUG_USING_UART ) || defined( RE_DEBUG_USING_UART_VERBOSE )
//      char	str_re_id[2];

//      itoa( 1+re_id, str_re_id, 10 );
        #endif

        // FIXME: the statement in the conditionals is constructed for
        // a active-low setup. implement active-high as well.

        // read last and current data bit pairs ( right shift inverted byte by 2 times re_id)

        // copy and invert
        tmp_data_last = ~re->data_last;
        tmp_data = ~re->data;

        // right shift by 2 times id
        tmp_data_last = ( tmp_data_last >> ( 2 * re_id ) );
        tmp_data = ( tmp_data >> ( 2 * re_id ) );

        // create clean byte with only bit 0 and 1 set
        if( tmp_data_last & ( 1 << 0 ) )
                re_data_last |= ( 1 << 0 );
        if( tmp_data_last & ( 1 << 1 ) )
                re_data_last |= ( 1 << 1 );
        if( tmp_data & ( 1 << 0 ) )
                re_data |= ( 1 << 0 );
        if( tmp_data & ( 1 << 1 ) )
                re_data |= ( 1 << 1 );

        #ifdef RE_DEBUG_USING_UART_VERBOSE
/*
        dbg_puts( "DEBUG: RE" );
        dbg_puts( str_re_id );
        dbg_puts( " -   re_data:    " );
        uart_puts_byte( ~re_data );
        dbg_puts( "   re_data_last:    " );
        uart_puts_byte( ~re_data_last );
        //dbg_puts( "\r\n" );
*/
        #endif /* RE_DEBUG_USING_UART_VERBOSE */

        // for Panasonic EVE
        if(     ( re_data_last == 0x00 && re_data == 0x01 ) ||
                ( re_data_last == 0x01 && re_data == 0x03 ) ||
                ( re_data_last == 0x03 && re_data == 0x02 ) ||
                ( re_data_last == 0x02 && re_data == 0x00 ) ) {
                // turned clock wise
                re_direction = RE_TURNED_CW;
        } else if(
                ( re_data_last == 0x00 && re_data == 0x02 ) ||
                ( re_data_last == 0x02 && re_data == 0x03 ) ||
                ( re_data_last == 0x03 && re_data == 0x01 ) ||
                ( re_data_last == 0x01 && re_data == 0x00 ) ) {
                // turned counter clock wise
                re_direction = RE_TURNED_CCW;
        }
        return re_direction;
}

void re_init( re_t *re )
{
        //
        // configure hardware
        //

        #if defined( RE_IO_TYPE_PARALLEL )
        // rotary encoder A/B pins are connected in parallel directly to the pins of the MCU

        // make lines A and B inputs (not required since input is the default configuration)
/*
        RE1_A_DDR &= ~( 1 << RE1_A );
        RE1_B_DDR &= ~( 1 << RE1_B );
        RE2_A_DDR &= ~( 1 << RE2_A );
        RE2_B_DDR &= ~( 1 << RE2_B );
*/
        #ifdef USE_INTERNAL_PULLUPS
        // activate internal pull-ups
        RE1_A_PORT |= ( 1 << RE1_A );
        RE1_B_PORT |= ( 1 << RE1_B );
        RE2_A_PORT |= ( 1 << RE2_A );
        RE2_B_PORT |= ( 1 << RE2_B );
        #endif /* USE_INTERNAL_PULLUPS */

        #elif defined( RE_IO_TYPE_SERIAL )
        // rotary encoder A/B pins are read seriall using a 74xx165 (parallel in / serial out shift register)

        // do the magic
        // optionally setup parallel in / serial out shift register if
        // or
        // store address of a function that has been registered to be called to read serial data
        // optionally parameters to the function

        #else
        #error "RE_IO_TYPE_PARALLEL or RE_IO_TYPE_SERIAL have to be defined."
        #endif /* RE_IO_TYPE_PARALLEL */

        //
        // setup variables
        //

        // initialize depending on active-high or
        // active-low configuration

        // FIXME make initialization for all encoders
        // and their corresponding A and B bits

        #ifdef RE1_ACTIVE_LOW
        // active-low
        re->data_last = 0xff;
        re->data = 0xff;
        #elif RE1_ACTIVE_HIGH
        // active-high
        re->data_last = 0x00;
        re->data = 0x00;
        #else
        #error "either RE1_ACTIVE_LOW or RE1_ACTIVE_HIGH must be defined."
        #endif

        /**
         \todo FIXME also initialize in serial io mode
         */

        #if defined( RE_IO_TYPE_PARALLEL )
        // initial read
        re->data = re_read_all( );
        // and store
        re->data_last = re->data;
        #endif /* defined/ RE_IO_TYPE_PARALLEL ) */

        #if RE_DATA_INTTYPE_WIDTH == 8
        re->changed = 0x00;
        #elif RE_DATA_INTTYPE_WIDTH == 16
        re->changed = 0x0000;
        #else
        #error "RE_DATA_INTTYPE_WIDTH must be defined (and either be 8 or 16)!"
        #endif

        re->lastKey = 0;
}

#if defined( RE_IO_TYPE_PARALLEL )
void re_update( re_t *re )
#elif defined( RE_IO_TYPE_SERIAL )
void re_update( re_t *re, const re_data_t data )
#else
#error "either RE_IO_TYPE_PARALLEL or RE_IO_TYPE_SERIAL must be defined."
#endif
{
        re_id_t         re_id;                  // iterator over rotary encoders
        re_data_t       re_data_last,           // place to store the A/B bit pair( 1=B, 0=A ) representing the last state
                        re_data,                // and one for the current state
                        tmp_data_last,          // used to prepare a new A/B bit pair, taken from re.data_last
                        tmp_data;               // and re.data
        re_direction_t  re_direction;           // signed int for the direction the rotary encoder is turned

        #if defined( RE_IO_TYPE_PARALLEL )
        // read all A and B pins for all encoders connected (lines are high by default and pulled low)
        re->data = re_read_all( );
        #elif defined( RE_IO_TYPE_SERIAL )
        re->data = data;
        #else
        #error "either RE_IO_TYPE_PARALLEL or RE_IO_TYPE_SERIAL must be defined."
        #endif

        // eval
        if( re->data_last != re->data )
        {
                // some rotary encoder changed
                //dbg_puts( "DEBUG: REx changed\r\n" );

                #if defined( RE_DEBUG_USING_UART ) || defined( RE_DEBUG_USING_UART_VERBOSE )
/*
                dbg_puts( "DEBUG: REx" );
                dbg_puts( " - re.data: " );
                uart_puts_byte( re->data );
*/
                #endif /* defined( RE_DEBUG_USING_UART ) || defined( RE_DEBUG_USING_UART_VERBOSE ) */

                #ifdef RE_DEBUG_USING_UART_VERBOSE
/*
                dbg_puts( " re.data_last:    " );
                uart_puts_byte( re->data_last );
                dbg_puts( "\r\n" );
*/
                #endif /* RE_DEBUG_USING_UART_VERBOSE */

                for( re_id = 0; re_id < RE_NUM; re_id++ )
                {
                        // FIXME this copy/right-shift/create code is quiet long
                        // find a nicer way to take two bits from a byte and put
                        // them in a new byte where the A/B bit pair is placed
                        // at the LSB side

                        re_data_last = 0;
                        re_data = 0;

                        // copy and invert
                        tmp_data_last = ~re->data_last;
                        tmp_data = ~re->data;

                        // right shift by 2 times id
                        tmp_data_last = ( tmp_data_last >> ( 2 * re_id ) );
                        tmp_data = ( tmp_data >> ( 2 * re_id ) );

                        // create clean byte with only bit 0 and 1 set
                        if( tmp_data_last & ( 1 << 0 ) )
                                re_data_last |= ( 1 << 0 );
                        if( tmp_data_last & ( 1 << 1 ) )
                                re_data_last |= ( 1 << 1 );
                        if( tmp_data & ( 1 << 0 ) )
                                re_data |= ( 1 << 0 );
                        if( tmp_data & ( 1 << 1 ) )
                                re_data |= ( 1 << 1 );

                        re_data_last = ~re_data_last;
                        re_data = ~re_data;

                        if( re_data_last != re_data )
                        {

                                // mark encoder id bit when data differs from data_last
                                re->changed |= ( 1 << re_id );


                                /*! \todo move call to re_tell_direction out of re_update into
                                 * the place that will process a "controls changed" event.
                                 */
                                re_direction = re_tell_direction( re, re_id );

                                if( re_direction == RE_TURNED_CW )
                                {
                                        handle_re_turned_cw( re, re_id );
                                        re->lastKey = (2*re_id);
                                } else if( re_direction == RE_TURNED_CCW ) {
                                        handle_re_turned_ccw( re, re_id );
                                        re->lastKey = (2*re_id)+1;

                                } else {
                                        // error - can't identify state transition
                                }

                                //
                                // debug output via uart
                                //
                                #if defined( RE_DEBUG_USING_UART ) || defined( RE_DEBUG_USING_UART_VERBOSE )
                                debug_re_to_uart( re, re_id, re_direction );
                                #endif /* defined( RE_DEBUG_USING_UART ) || defined( RE_DEBUG_USING_UART_VERBOSE ) */
                        }
                }

                #ifdef RE_DEBUG_USING_LEDS
                debug_byte_to_leds( re->data );
                #endif /* RE_DEBUG_USING_LEDS */

                // store as last
                re->data_last = re->data;
        }
}

/* vim: set expandtab: */
