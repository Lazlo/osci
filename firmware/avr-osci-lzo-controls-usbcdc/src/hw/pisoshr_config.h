/**
 * \file pisoshr_config.h
 */

#ifndef _PISOSHR_CONFIG_H
#define _PISOSHR_CONFIG_H

#if defined(__AVR_ATmega8__)
// clock output
#define PISOSHR_CLK             PB2
#define PISOSHR_CLK_DDR         DDRB
#define PISOSHR_CLK_PORT        PORTB

// serial input
#define PISOSHR_Q               PB1
#define PISOSHR_Q_DDR           DDRB
#define PISOSHR_Q_PIN           PINB

// parallel enable output
#define PISOSHR_SH_LD           PB0
#define PISOSHR_SH_LD_DDR       DDRB
#define PISOSHR_SH_LD_PORT      PORTB
#endif /* __AVR_ATmega8__ */

#if defined(__AVR_ATmega32__)
// clock output
#define PISOSHR_CLK             PD5
#define PISOSHR_CLK_DDR         DDRD
#define PISOSHR_CLK_PORT        PORTD

// serial input
#define PISOSHR_Q               PD6
#define PISOSHR_Q_DDR           DDRD
#define PISOSHR_Q_PIN           PIND

// parallel enable output
#define PISOSHR_SH_LD           PD7
#define PISOSHR_SH_LD_DDR       DDRD
#define PISOSHR_SH_LD_PORT      PORTD
#endif /* __AVR_ATmega32__ */

#if defined(__AVR_ATmega644__)
// clock output
#define PISOSHR_CLK             PA5
#define PISOSHR_CLK_DDR         DDRA
#define PISOSHR_CLK_PORT        PORTA

// serial input
#define PISOSHR_Q               PA6
#define PISOSHR_Q_DDR           DDRA
#define PISOSHR_Q_PIN           PINA

// parallel enable output
#define PISOSHR_SH_LD           PA7
#define PISOSHR_SH_LD_DDR       DDRA
#define PISOSHR_SH_LD_PORT      PORTA
#endif /* defined(__AVR_ATmega644__) */

#endif /* _PISOSHR_CONFIG_H */

/* vim: set expandtab: */
