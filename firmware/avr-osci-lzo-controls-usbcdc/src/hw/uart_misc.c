/**
 * \file uart_misc.c
 */

#include "uart.h"

void uart_puts_byte( uint8_t byte ) {
	int i;
	// start with MSB
	for( i = 7; i >= 0; i-- ) {
		if( byte & ( 1 << i ) )
			uart_puts( "1" );
		else
			uart_puts( "0" );
	}
}
