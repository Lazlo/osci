/**
 * \file rotary_encoder.h
 */

#ifndef _ROTARY_ENCODER_H
#define _ROTARY_ENCODER_H

#include <inttypes.h>			// for uint8_t and uint16_t
#include "rotary_encoder_config.h"	// for RE_IO_TYPE_PARALLEL and RE_IO_TYPE_SERIAL

/*! \brief rotary encoder turned clock wise */
#define RE_TURNED_CW	1
/*! \brief rotary encoder turned counter clock wise */
#define RE_TURNED_CCW	-1

#if RE_DATA_INTTYPE_WIDTH == 8
/*! \brief rotary encoder state data type (8-bit) */
typedef uint8_t re_data_t;
#elif RE_DATA_INTTYPE_WIDTH == 16
/*! \brief rotary encoder state data type (16-bit) */
typedef uint16_t re_data_t;
#else
#error "RE_DATA_INTTYPE_WIDTH must be 8 or 16"
#endif

/*! \brief rotary encoder id type */
typedef uint8_t re_id_t;

/*! \brief rotary encoder direction type */
typedef int8_t	re_direction_t;

/**
 * \brief contains last and current A/B states of N rotary encoders
 */
typedef struct {
	/*! \brief rotary encoder states from last read operation */
	re_data_t	data_last,		// storage for last A/B bit pairs
	/*! \brief rotary encoder states from current read operation */
			data,			// storage for current A/B bit pairs
	/*! \brief has any rotary encoder been turned? each bit marks a rotary encoder id. */
			changed;
	/*! \brief rotary encoder values */
	/*! \todo feeling tells me this is not the right place to keep those values */
	uint8_t		value[RE_NUM];		// place for values that the get altered by moving a particular encoder
        /*! \brief id of last key that changed */
        unsigned char   lastKey;
} re_t;

#if defined( RE_DEBUG_USING_LEDS )
/*! \brief display byte using LEDs */
void debug_byte_to_leds( const uint8_t byte );
#endif /* defined( RE_DEBUG_USING_LEDS ) */

#if defined( RE_IO_TYPE_PARALLEL )
/*! \brief read states of all rotary encoders (only in parallel I/O mode) */
re_data_t re_read_all( void );
#endif /* defined( RE_IO_TYPE_PARALLEL ) */

/*! \brief identify direction a rotary encoder was turned */
re_direction_t re_tell_direction( const re_t *re, const re_id_t re_id );

/*! \brief initialize rotary encoder data structure */
void re_init( re_t *re );

#if defined( RE_IO_TYPE_PARALLEL )
/*! \brief update rotary encoder data (will call re_read_all in parallel I/O mode) */
void re_update( re_t *re );
#elif defined( RE_IO_TYPE_SERIAL )
/*! \brief update rotary encoder */
void re_update( re_t *re, re_data_t re_data );
#else
#error "either RE_IO_TYPE_PARALLEL or RE_IO_TYPE_SERIAL must be defined."
#endif

#endif /* _ROTARY_ENCODER_H */

/* vim: set expandtab: */
