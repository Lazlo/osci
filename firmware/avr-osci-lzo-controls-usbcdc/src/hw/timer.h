/**
 * \file timer.h
 */

#ifndef _TIMER_H
#define _TIMER_H

#include <avr/io.h>

///////////////////////////////////////////////////////////////////////////////
//
// 8-bit Timer/Counter0 Registers and Bits
//
///////////////////////////////////////////////////////////////////////////////

#if defined(__AVR_ATmega644__)

#define TIMER_COUNTER_CONTROL_REGISTER_A                        TCCR0A  // 8-bits, bit 3 and 2 reserved
#define TIMER_COMPARE_MATCH_OUTPUT_A_MODE_BIT1                  COM0A1  // 7
#define TIMER_COMPARE_MATCH_OUTPUT_A_MODE_BIT0                  COM0A0  // 6
#define TIMER_COMPARE_MATCH_OUTPUT_B_MODE_BIT1                  COM0B1  // 5
#define TIMER_COMPARE_MATCH_OUTPUT_B_MODE_BIT0                  COM0B0  // 4
#define TIMER_WAVEFORM_GENERATOR_MODE_BIT1                      WGM01   // 1
#define TIMER_WAVEFORM_GENERATOR_MODE_BIT0                      WGM00   // 0

#define TIMER_COUNTER_CONTROL_REGISTER_B                        TCCR0B  // 8-bits, bit 5 and 4 reserved
#define TIMER_FORCE_OUTPUT_COMPARE_A_BIT                        FOC0A   // 7
#define TIMER_FORCE_OUTPUT_COMPARE_B_BIT                        FOC0B   // 6
#define TIMER_WAVEFORM_GENERATOR_MODE_BIT2                      WGM02   // 3
#define TIMER_CLOCK_SELECT_BIT2                                 CS02    // 2
#define TIMER_CLOCK_SELECT_BIT1                                 CS01    // 1
#define TIMER_CLOCK_SELECT_BIT0                                 CS00    // 0

#define TIMER_COUNTER_REGISTER                                  TCNT0   // 8-bits
#define OUTPUT_COMPARE_REGISTER_A                               OCR0A   // 8-bits
#define OUTPUT_COMPARE_REGISTER_B                               OCR0B   // 8-bits

#define TIMER_COUNTER_INTERRUPT_MASK_REGISTER                   TIMSK0  // 8-bits, bit 7 to 3 reserved
#define TIMER_COUNTER_OUTPUT_COMPARE_MATCH_B_INTERRUPT_ENABLE   OCIE0B  // 2
#define TIMER_COUNTER_OUTPUT_COMPARE_MATCH_A_INTERRUPT_ENABLE   OCIE0A  // 1
#define TIMER_COUNTER_OVERFLOW_INTERRUPT_ENABLE                 TOIE0   // 0

#define TIMER_COUNTER_INTERRUPT_FLAG_REGISTER                   TIFR0   // 8-bits, bit 7 to 3 reserved
#define TIMER_COUNTER_OUTPUT_COMPARE_MATCH_B_FLAG               OCF0B   // 2
#define TIMER_COUNTER_OUTPUT_COMPARE_MATCH_A_FLAG               OCF0A   // 1
#define TIMER_COUNTER_OVERFLOW_FLAG                             TOV0    // 0

#endif /* defined(__AVR_ATmega644__) */

#if defined(__AVR_ATmega32__) | defined(__AVR_ATmega16__)

#define TIMER_COUNTER_CONTROL_REGISTER                          TCCR0   // 8-bits
#define TIMER_FORCE_OUTPUT_COMPARE_BIT                          FOC0    // 7
#define TIMER_WAVEFORM_GENERATOR_MODE_BIT0                      WGM00   // 6
#define TIMER_COMPARE_MATCH_OUTPUT_MODE_BIT1                    COM01   // 5
#define TIMER_COMPARE_MATCH_OUTPUT_MODE_BIT0                    COM00   // 4
#define TIMER_WAVEFORM_GENERATOR_MODE_BIT1                      WGM01   // 3
#define TIMER_CLOCK_SELECT_BIT2                                 CS02    // 2
#define TIMER_CLOCK_SELECT_BIT1                                 CS01    // 1
#define TIMER_CLOCK_SELECT_BIT0                                 CS00    // 0

#define TIMER_COUNTER_REGISTER                                  TCNT0   // 8-bits
#define OUTPUT_COMPARE_REGISTER                                 OCR0    // 8-bits

#define TIMER_COUNTER_INTERRUPT_MASK_REGISTER                   TIMSK   // 8-bits
#define TIMER_COUNTER_OUTPUT_COMPARE_MATCH_INTERRUPT_ENABLE     OCIE0   // 1
#define TIMER_COUNTER_OVERFLOW_INTERRUPT_ENABLE                 TOIE0   // 0

#define TIMER_COUNTER_INTERRUPT_FLAG_REGISTER                   TIFR    // 8-bits
#define TIMER_COUNTER_OUTPUT_COMPARE_FLAG                       OCF0    // 1
#define TIMER_COUNTER_OVERFLOW_FLAG                             TOV0    // 0

#endif /* defined(__AVR_ATmega32__) | defined(__AVR_ATmega16__) */

#if defined(__AVR_ATmega8__)

/**
 * Timer 0
 */
#define TIMER_COUNTER_CONTROL_REGISTER                          TCCR0   // 8-bits, bit 7 to 3 reserved
#define TIMER_CLOCK_SELECT_BIT2                                 CS02    // 2
#define TIMER_CLOCK_SELECT_BIT1                                 CS01    // 1
#define TIMER_CLOCK_SELECT_BIT0                                 CS00    // 0

#define TIMER_COUNTER_REGISTER                                  TCNT0   // 8-bits

#define TIMER_COUNTER_INTERRUPT_MASK_REGISTER                   TIMSK   // 8-bits
#define TIMER_COUNTER_OVERFLOW_INTERRUPT_ENABLE                 TOIE0   // 0

#define TIMER_COUNTER_INTERRUPT_FLAG_REGISTER                   TIFR    // 8-bits
#define TIMER_COUNTER_OVERFLOW_FLAG                             TOV0    // 0

/**
 * Timer 2
 */
#define TIMER2_COUNTER_CONTROL_REGISTER                         TCCR2   // 8-bits
#define TIMER2_FORCE_OUTPUT_COMPARE_BIT                         FOC2    // 7
#define TIMER2_WAVEFORM_GENERATION_MODE_BIT0                    WGM20   // 6
#define TIMER2_COMPARE_MATCH_OUTPUT_MODE_BIT1                   COM21   // 5
#define TIMER2_COMPARE_MATCH_OUTPUT_MODE_BIT0                   COM20   // 4
#define TIMER2_WAVEFORM_GENERATION_MODE_BIT1                    WGM21   // 3
#define TIMER2_CLOCK_SELECT_BIT2                                CS22    // 2
#define TIMER2_CLOCK_SELECT_BIT1                                CS21    // 1
#define TIMER2_CLOCK_SELECT_BIT0                                CS20    // 0

#define TIMER2_COUNTER_REGISTER                                 TCNT2   // 8-bits

#define TIMER2_COUNTER_OUTPUT_COMPARE_REGISTER                  OCR2    // 8-bits

#define TIMER2_COUNTER_INTERRUPT_MASK_REGISTER                  TIMER_COUNTER_INTERRUPT_MASK_REGISTER
#define TIMER2_COUNTER_OUTPUT_COMPARE_MATCH_INTERRUPT_ENABLE    OCIE2   // 7
#define TIMER2_COUNTER_OVERFLOW_INTERRUPT_ENABLE                TOIE2   // 6
#endif // defined(__AVR_ATmega8

///////////////////////////////////////////////////////////////////////////////
//
// Output Compare Pin Mapping
//
///////////////////////////////////////////////////////////////////////////////

#if defined(__AVR_ATmega644__)

#define OUTPUT_COMPARE_0A               PB3     // OC0A
#define OUTPUT_COMPARE_0A_DDR           DDRB
#define OUTPUT_COMPARE_0B               PB4     // OC0B
#define OUTPUT_COMPARE_0B_DDR           DDRB
#define OUTPUT_COMPARE_1A               PD5     // OC1A
#define OUTPUT_COMPARE_1A_DDR           DDRD
#define OUTPUT_COMPARE_1B               PD4     // OC1B
#define OUTPUT_COMPARE_1B_DDR           DDRD
#define OUTPUT_COMPARE_2A               PD7     // OC2A
#define OUTPUT_COMPARE_2A_DDR           DDRD
#define OUTPUT_COMPARE_2B               PD6     // OC2B
#define OUTPUT_COMPARE_2B_DDR           DDRD

#endif /* defined(__AVR_ATmega644__) */

#if defined(__AVR_ATmega32__) | defined(__AVR_ATmega16__)

#define OUTPUT_COMPARE_0                PB3     // OC0
#define OUTPUT_COMPARE_0_DDR            DDRB
#define OUTPUT_COMPARE_1A               PD5     // OC1A
#define OUTPUT_COMPARE_1A_DDR           DDRD
#define OUTPUT_COMPARE_1B               PD4     // OC1B
#define OUTPUT_COMPARE_1B_DDR           DDRD
#define OUTPUT_COMPARE_2                PD7     // OC2
#define OUTPUT_COMPARE_2_DDR            DDRD

#endif /* defined(__AVR_ATmega32__) | defined(__AVR_ATmega16__) */

///////////////////////////////////////////////////////////////////////////////
//
// Macros
//
///////////////////////////////////////////////////////////////////////////////

#if defined(__AVR_ATmega32__) | defined(__AVR_ATmega16__)

// Wave Generation Mode
#define TIMER_COUNTER_NORMAL_MODE               0       // 0 0
#define TIMER_COUNTER_PHASE_CORRECT_PWM_MODE    1       // 0 1
#define TIMER_COUNTER_CTC_MODE                  2       // 1 0
#define TIMER_COUNTER_FAST_PWM_MODE             3       // 1 1

// Compare Output Mode

// Compare Output Mode, non-PWM Mode
// 0 0	Normal port operation, OC0 disconnected.
// 0 1	Toggle OC0 on compare match
// 1 0	Clear OC0 on compare match
// 1 1	Set OC0 on compare match

// Compare Output Mode, Fast PWM Mode
// 0 0	Normal port operation, 0C0 disconnected.
// 0 1	Reserved
// 1 0	Clear OC0 on compare match, set OC0 at BOTTOM (non-inverting mode)
// 1 1	Set OC0 on compare match, clear OC0 at BOTTOM (inverting mode)

// Compare Output Mode, Phase Correct PWM Mode
// 0 0	Normal port operation, OC0 disconnected.
// 0 1	Reserved
// 1 0	Clear OC0 on compare match when up-counting. Set OC0 on compare match when down-counting.
// 1 1	Set OC0 on compare match when up-counting. Clear OC0 on compare match when down-counting.



#endif /* defined(__AVR_ATmega32__) | defined(__AVR_ATmega16__) */

///////////////////////////////////////////////////////////////////////////////

// Clock Select
/*! \brief no clock source */
#define TIMER_CLOCK_STOP                0
/*! \brief internal clock */
#define TIMER_CLOCK_DIV_1               1
/*! \brief internal clock with prescaler 8 */
#define TIMER_CLOCK_DIV_8               2
/*! \brief internal clock with prescaler 64 */
#define TIMER_CLOCK_DIV_64              3
/*! \brief internal clock with prescaler 256 */
#define TIMER_CLOCK_DIV_256             4
/*! \brief internal clock with prescaler 1024 */
#define TIMER_CLOCK_DIV_1024            5
/*! \brief external clock from T0 pin at falling edge */
#define TIMER_CLOCK_T0_FALLING          6
/*! \brief external clock from T0 pin at rising edge */
#define TIMER_CLOCK_T0_RISING           7

/*! \brief enable global interrupts */
#define INTERRUPTS_ENABLE sei( )
/*! \brief disable global interrupts */
#define INTERRUPTS_DISABLE cli( )

/*! \brief set top value for counter */
void timer_set_top( unsigned char timer, uint8_t top );
/*! \brief set wave generation mode */
void timer_set_wave_generation_mode( unsigned char timer, int wgm );
/*! \brief set output compare mode */
void timer_set_output_compare_mode( unsigned char timer );
/*! \brief select clock source */
void timer_set_clock_source( unsigned char timer, int cs );
/*! \brief enable overflow interrupt */
void timer_interrupt_enable( unsigned char timer );
/*! \brief initialize timer */
void timer_init( unsigned char timer );

#endif /* _TIMER_H */

/* vim: set expandtab: */
