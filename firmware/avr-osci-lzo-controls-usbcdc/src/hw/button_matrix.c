/**
 * \file button_matrix.c
 */

#include "button_matrix.h"

#if defined( DEBUG_BUTTON_MATRIX ) || defined( DEBUG_BUTTON_MATRIX_TICKS )
#include "uart.h"

#define BM_OUTPUT_STR(s) uart_puts(s)
#define dbg_puts(s) BM_OUTPUT_STR(s)

#endif /* defined( DEBUG_BUTTON_MATRIX ) || defined( DEBUG_BUTTON_MATRIX_TICKS ) */
#if defined( DEBUG_BUTTON_MATRIX )
#include "../strutils.h"
#endif /* defined( DEBUG_BUTTON_MATRIX ) */

bm_key_id_t button_matrix_get_key( uint8_t column, uint8_t row )
{
        return column + ( row * 5 ) + 1;
}

#ifdef BUTTON_MATRIX_INCLUDE_BUTTON_NAMES

char * button_matrix_get_key_name( bm_key_id_t key_id )
{
        switch( key_id ) {
        case KEY_UNKNOWN:               return "unknown";
        case KEY_SAVE_RECALL:           return "Save / Recall";
        case KEY_MEASURE:               return "Measure";
        case KEY_ACQUIRE:               return "Acquire";
        case KEY_DEFAULT_SETUP:         return "Default Setup";
        case KEY_SINGLE:                return "Single";
        case KEY_UTILITY:               return "Utility";
        case KEY_CURSOR:                return "Cursor";
        case KEY_DISPLAY:               return "Display";
        case KEY_HELP:                  return "Help";
        case KEY_RUN_STOP:              return "Run / Stop";
        case KEY_CH1_MENU:              return "Ch1 Menu";
        case KEY_MATH:                  return "Math";
        case KEY_CH2_MENU:              return "Ch2 Menu";
        case KEY_HORIZONTAL_MENU:       return "Horizonzal Menu";
        case KEY_TRIGGER_MENU:          return "Trigger Menu";
        case KEY_REF:                   return "Ref";
        case KEY_FORCE_TRIGGER:         return "Force Trigger";
        case KEY_CH2_VDIV:              return "Ch2 V/Div";
        case KEY_HORIZONTAL_SDIV:       return "Horizontal S/Div";
        case KEY_TRIGGER_LEVEL:         return "Trigger Level";
        case KEY_CH1_VDIV:              return "Ch1 V/Div";
        case KEY_CH1_POSITION:          return "Ch1 Position";
        case KEY_CH2_POSITION:          return "Ch2 Position";
        case KEY_HORIZONTAL_POSITION:   return "Horizontal Position";
        case KEY_GENERAL_PURPOSE:       return "General Purpose Knob";
        default:                        return "";
        }
}

#endif /* BUTTON_MATRIX_INCLUDE_BUTTON_NAMES */


void button_matrix_handle_key_change( uint8_t column, uint8_t row, bm_event_t bm_event )
{
        bm_key_id_t key_id;
#ifdef DEBUG_BUTTON_MATRIX
        #ifdef BUTTON_MATRIX_INCLUDE_COL_ROW_PAIR
        char            str_col[2],
                        str_row[2];
        #endif /* BUTTON_MATRIX_INCLUDE_COL_ROW_PAIR */
        char            str_key_id[3];

        #ifdef BUTTON_MATRIX_INCLUDE_COL_ROW_PAIR
        itoa( 1+column, str_col, 10 );
        itoa( row+1, str_row, 10 );
        #endif /* BUTTON_MATRIX_INCLUDE_COL_ROW_PAIR */

        #ifdef DEBUG_BUTTON_MATRIX_TICKS
        dbg_puts( "\r\n" );
        #endif /* DEBUG_BUTTON_MATRIX_TICKS */

        key_id = button_matrix_get_key( column, row );
        itoa( key_id, str_key_id, 10 );

        dbg_puts( "D: K" );
        dbg_puts( str_key_id );
        dbg_puts( " " );

        #ifdef BUTTON_MATRIX_INCLUDE_COL_ROW_PAIR
        dbg_puts("(c=" );
        dbg_puts( str_col );
        dbg_puts( ",r=" );
        dbg_puts( str_row );
        dbg_puts( ") " );
        #endif /* BUTTON_MATRIX_INCLUDE_COL_ROW_PAIR */

        #ifdef BUTTON_MATRIX_INCLUDE_BUTTON_NAMES
        dbg_puts( "\"" );
        dbg_puts( button_matrix_get_key_name( key_id ) );
        dbg_puts( "\" " );
        #endif /* BUTTON_MATRIX_INCLUDE_BUTTON_NAMES */

        #ifndef BUTTON_MATRIX_KEY_PRESS_ONLY
        if( bm_event == BUTTON_MATRIX_EVENT_KEY_PRESSED )
                dbg_puts( "pressed" );
        else if( bm_event == BUTTON_MATRIX_EVENT_KEY_RELEASED )
                dbg_puts( "released" );
        else
                dbg_puts( "error" );
        #endif /* BUTTON_MATRIX_KEY_PRESS_ONLY */
        dbg_puts( "\r\n" );
#endif /* DEBUG_BUTTON_MATRIX */
        return;
}

void button_matrix_init( bm_t *bm )
{
        uint8_t column;
        // init button matrix
        for( column = 0; column < BUTTON_MATRIX_COLS; column++ )
                bm->last[column] = 0;
        bm->lastKey = 0;
        bm->changed = 0;
}

void button_matrix_update( bm_t *bm, uint8_t column, uint8_t column_data )
{
        uint8_t         row,
                        last_column_data;

        // copy content of last state for current column
        last_column_data = bm->last[column];

        // state changed?
        if( column_data != last_column_data )
        {
                bm->changed = 1;
//              dbg_puts( "\r\nDEBUG: something has changed!\r\n" );
                for( row = 0; row < BUTTON_MATRIX_ROWS; row++ )
                {
                        // pressed
                        if( !( last_column_data & ( 1 << (7-row) ) ) && ( column_data & ( 1 << (7-row) ) ) ) {
                                button_matrix_handle_key_change( column, row, BUTTON_MATRIX_EVENT_KEY_PRESSED );
                                bm->lastKey = button_matrix_get_key( column, row );
                        }
#ifndef BUTTON_MATRIX_KEY_PRESS_ONLY
                        // released
                        if( ( last_column_data & ( 1 << (7-row) ) ) && !( column_data & ( 1 << (7-row) ) ) ) {
                                button_matrix_handle_key_change( column, row, BUTTON_MATRIX_EVENT_KEY_RELEASED );
                        }
#endif
                }
                // store current state
                bm->last[column] = column_data;
        }
#ifdef DEBUG_BUTTON_MATRIX_TICKS
        dbg_puts( "." );
#endif /* DEBUG_BUTTON_MATRIX_TICKS */
}

/* vim: set expandtab: */
