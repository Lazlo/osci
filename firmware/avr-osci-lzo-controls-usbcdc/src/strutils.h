/**
 * \file strutils.h
 */

#ifndef _STRUTILS_H
#define _STRUTILS_H

/*! \brief reverse character string */
void strreverse( char *begin, char *end );

/*! \brief transform integer to character string */
void itoa( int value, char *str, int base );

#endif /* _STRUTILS_H */
